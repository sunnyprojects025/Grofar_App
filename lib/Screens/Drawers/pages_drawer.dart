import 'package:flutter/material.dart';
import 'package:grofar/Screens/DrawerItemScreens/DefaulScreen.dart';
import 'package:grofar/Screens/DrawerItemScreens/help&ticket_screen.dart';
import 'package:grofar/Screens/promos_page_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/termsConditions_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/faq_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/help&Support_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/privacy_screen.dart';

import '../promos_screen.dart';

class PagesDrawer extends StatelessWidget {
  final List draweItem = [
    {'title': 'AskBootstrap', 'widget': DefaultScreen()},
    {'title': 'Promos', 'widget': PromosScreen()},
    {'title': 'Promo Details', 'widget': PromosPageScreen()},
    {'title': 'Terms & Conditions', 'widget': TermsConditionsScreen()},
    {'title': 'Privacy', 'widget': PrivacyScreen()},
    {'title': 'Conditions', 'widget': TermsConditionsScreen()},
    {
      'title': 'Help Support',
      'widget': HelpSupportScreen(
        title: 'Help & Support',
      )
    },
    {'title': 'Help Ticket', 'widget': HelpTicketScreen()},
    {'title': 'Refund Payment', 'widget': HelpTicketScreen()},
    {'title': 'FAQ', 'widget': FAQScreen()},
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Material(
      child: Scaffold(
        backgroundColor: Colors.black.withOpacity(.3),
        body: SafeArea(
          child: Container(
            height: double.infinity,
            color: Colors.white,
            width: deviceSize.width * .7,
            child: Column(
              children: [
                Container(
                  height: deviceSize.height / 4 / 2,
                  width: double.infinity,
                  color: Color(0xFF343A40),
                  child: Container(
                    margin: EdgeInsets.only(
                      top: deviceSize.height / 4 / 4 * .7,
                      left: 15,
                    ),
                    child: Text(
                      'Pages',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                ),
                Expanded(
                    child: ListView(children: [
                  for (int index1 = 0; index1 < draweItem.length; index1++)
                    GestureDetector(
                      child: Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: ListTile(
                            leading: Text(
                          draweItem[index1]['title'],
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w500),
                        )),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.grey.withOpacity(.7), width: .1)),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (ctx) {
                          return draweItem[index1]['widget'] == null
                              ? null
                              : draweItem[index1]['widget'];
                        }));
                      },
                    )
                ])),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
