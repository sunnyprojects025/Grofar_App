import 'package:flutter/material.dart';
import 'package:grofar/Screens/DrawerItemScreens/DefaulScreen.dart';
import 'package:grofar/Screens/DrawerItemScreens/changeDelivery_time_Screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/mostPopular_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/notification_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/order_placed_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/selectAddress_screen.dart';
import 'package:grofar/Screens/Drawers/pages_drawer.dart';
import 'package:grofar/Screens/bottomNavigationScreens/cartScreen.dart';
import 'package:grofar/Screens/bottomNavigation_Screen.dart';
import 'package:grofar/Screens/feedback_screen.dart';
import 'package:grofar/Screens/lookingForItem_screen.dart';
import 'package:grofar/Screens/search_screen.dart';

class DrawerScreen extends StatelessWidget {
  final List draweItem = [
    {
      'icon': Icon(Icons.home),
      'title': 'Homepage',
      'widget': BottomNavigationScreen()
    },
    {
      'icon': Icon(Icons.notification_important_sharp),
      'title': 'notifications',
      'widget': NotifiactionScreen()
    },
    {'icon': Icon(Icons.search), 'title': 'Search', 'widget': SearchScreen()},
    {
      'icon': Icon(Icons.book),
      'title': 'Listing',
      'widget': LookingForItemScreen()
    },
    {
      'icon': Icon(Icons.trending_down_sharp),
      'title': 'Trending',
      'widget': DefaultScreen()
    },
    {
      'icon': Icon(Icons.thumb_up),
      'title': 'Recommended',
      'widget': DefaultScreen()
    },
    {
      'icon': Icon(Icons.tag),
      'title': 'Most Popular',
      'widget': MostPopularScreen()
    },
    {
      'icon': Icon(Icons.search_off_rounded),
      'title': 'Product Details',
      'widget': DefaultScreen()
    },
    {
      'icon': Icon(Icons.shopping_cart),
      'title': 'Cart',
      'widget': CartScreen()
    },
    {
      'icon': Icon(Icons.location_city),
      'title': 'Order Address',
      'widget': SelectAddressScreen()
    },
    {
      'icon': Icon(Icons.calendar_today),
      'title': 'Delivery Time',
      'widget': ChangeDeliveryAddressScreen()
    },
    {
      'icon': Icon(Icons.money),
      'title': 'Order Payment',
      'widget': OrderPlacedScreen()
    },
    {
      'icon': Icon(Icons.check_outlined),
      'title': 'Checkout',
      'widget': CartScreen()
    },
    {
      'icon': Icon(Icons.pages),
      'title': 'Pages',
      'widget': PagesDrawer(),
    },
    {
      'icon': Icon(Icons.feedback),
      'title': 'Feedback',
      'widget': FeedBackScreen()
    },
  ];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      key: _scaffoldKey,
      drawer: PagesDrawer(),
      body: Container(
        height: double.infinity,
        child: Column(
          children: [
            Expanded(
                child: ListView(
              children: [
                for (int index1 = 0; index1 < draweItem.length; index1++)
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: ListTile(
                        leading: Wrap(
                          children: [
                            FlatButton.icon(
                                onPressed: () {
                                  if (draweItem[index1]['widget'] ==
                                      PagesDrawer()) {
                                    return _scaffoldKey.currentState
                                        .openDrawer();
                                  } else {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (ctx) {
                                      return draweItem[index1]['widget'];
                                    }));
                                  }
                                },
                                icon: draweItem[index1]['icon'],
                                label: Text(draweItem[index1]['title']))
                          ],
                        ),
                      ),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.grey.withOpacity(.7), width: .1)),
                    ),
                    onTap: () {
                      if (draweItem[index1]['widget'] == PagesDrawer()) {
                        return _scaffoldKey.currentState.openDrawer();
                      } else {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (ctx) {
                          return draweItem[index1]['widget'];
                        }));
                      }
                    },
                  ),
              ],
            )),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    child: Column(
                      children: [
                        Icon(Icons.home),
                        SizedBox(
                          height: 10,
                        ),
                        Text('Home'),
                      ],
                    ),
                  ),
                  GestureDetector(
                    child: Column(
                      children: [
                        Icon(Icons.shopping_basket_sharp),
                        SizedBox(
                          height: 10,
                        ),
                        Text('Cart'),
                      ],
                    ),
                  ),
                  GestureDetector(
                    child: Column(
                      children: [
                        Icon(Icons.music_off),
                        SizedBox(
                          height: 10,
                        ),
                        Text('Help'),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
