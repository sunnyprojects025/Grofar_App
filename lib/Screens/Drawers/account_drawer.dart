import 'package:flutter/material.dart';
import 'package:grofar/Screens/DrawerItemScreens/DefaulScreen.dart';
import 'package:grofar/Screens/DrawerItemScreens/changePassword_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/deactivateAccount_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/editProfile_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/selectAddress_screen.dart';
import 'package:grofar/Screens/bottomNavigationScreens/accountScreen.dart';
import 'package:grofar/Screens/bottomNavigationScreens/myOrder_Screen.dart';

class AccountDrawer extends StatelessWidget {
  final List draweItem = [
    {'title': 'AskBootstrap', 'widget': DefaultScreen()},
    {'title': 'My Account', 'widget': AccountScreen()},
    {'title': 'Edit Profile', 'widget': EditProfileScreen()},
    {'title': 'Change Password', 'widget': ChangePasswordScreen()},
    {'title': 'Progress Order', 'widget': MyOrderScreen()},
    {'title': 'Status On process', 'widget': MyOrderScreen()},
    {'title': 'Deactivate', 'widget': DeactivateAccountScreen()},
    {'title': 'My Address', 'widget': SelectAddressScreen()},
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
      height: double.infinity,
      child: Column(
        children: [
          Container(
            height: deviceSize.height / 4 / 2,
            width: double.infinity,
            color: Color(0xFF343A40),
            child: Container(
              margin: EdgeInsets.only(
                top: deviceSize.height / 4 / 4 * .7,
                left: 15,
              ),
              child: Text(
                'My Account',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
            ),
          ),
          Expanded(
              child: ListView(children: [
            for (int index1 = 0; index1 < draweItem.length; index1++)
              GestureDetector(
                child: Container(
                  padding: EdgeInsets.only(top: 5, bottom: 5),
                  child: ListTile(
                      leading: Text(
                    draweItem[index1]['title'],
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w500),
                  )),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey.withOpacity(.7), width: .1)),
                ),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                    return draweItem[index1]['widget'] == null
                        ? null
                        : draweItem[index1]['widget'];
                  }));
                },
              )
          ])),
        ],
      ),
    );
  }
}
