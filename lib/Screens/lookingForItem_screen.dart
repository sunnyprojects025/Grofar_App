import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/Drawers/Drawer.dart';
import 'package:grofar/Screens/filterScreen.dart';

class LookingForItemScreen extends StatefulWidget {
  final String title;

  LookingForItemScreen({this.title});

  @override
  _LookingForItemScreenState createState() => _LookingForItemScreenState();
}

class _LookingForItemScreenState extends State<LookingForItemScreen> {
  List tiles = [
    {'image': 'assets/img/green/g1.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g2.jpg', 'item': 'Chili'},
    {'image': 'assets/img/green/g3.jpg', 'item': 'Onion'},
    {'image': 'assets/img/green/g4.jpg', 'item': 'Cabbage'},
    {'image': 'assets/img/green/g5.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g6.jpg', 'item': 'Jamun'},
    {'image': 'assets/img/green/g7.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g8.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g9.jpg', 'item': 'Friuts'},
  ];

  int _selectedPageIndex = 0;

  // List<Map<String, Object>> _pages = [
  //   {
  //     'page': FilterScreen(),
  //     'title': 'Categories',
  //   },
  //   {
  //     'page': FilterScreen(),
  //     'title': 'Your Favorite',
  //   },
  // ];
  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
      Navigator.push(context, MaterialPageRoute(builder: (ctx) {
        return FilterScreen();
      }));
    });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: DrawerScreen(),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: ListView(
            children: [
              Card(
                elevation: 1,
                child: Container(
                  width: double.infinity,
                  height: deviceSize.height / 4 / 3,
                  color: Color(0xFFF0F2F5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            IconButton(
                                icon: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.green,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                }),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              widget.title == null ? 'Vegetable' : widget.title,
                              style: TextStyle(
                                  color: Color(0xFF212529),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20),
                            )
                          ],
                        ),
                      ),
                      GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(right: 10),
                          padding: EdgeInsets.all(5),
                          child: Icon(Icons.menu),
                        ),
                        onTap: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                      )
                    ],
                  ),
                ),
              ),
              Column(children: [
                GridView(
                  scrollDirection: Axis.vertical,
                  physics: BouncingScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 4 / 5,
                  ),
                  shrinkWrap: true,
                  children: <Widget>[
                    for (int index1 = 0; index1 < tiles.length; index1++)
                      Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                            padding: EdgeInsets.all(5),
                            child: Column(children: [
                              Container(
                                width: deviceSize.width / 2 * .7,
                                height: deviceSize.height / 4 * .7,
                                child: Stack(
                                  children: [
                                    Positioned(
                                        top: 10,
                                        left: 5,
                                        child: Container(
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: Colors.orangeAccent[100],
                                          ),
                                          child: Text(
                                            "25%",
                                            style: TextStyle(
                                                color: Colors.orange[700]),
                                          ),
                                        )),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image:
                                            AssetImage(tiles[index1]['image']),
                                        fit: BoxFit.fill)),
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(5),
                                      margin:
                                          EdgeInsets.only(left: 10, top: 10),
                                      child: Text(
                                        tiles[index1]['item'],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xFF212529),
                                            fontSize: 16),
                                      ),
                                    ),
                                  ]),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(10),
                                    child: Text(
                                      '\$ 1/kg',
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                  ),
                                  GestureDetector(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 10),
                                      width: 40,
                                      height: 40,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color: Color(0xFFF0F2F5),
                                        shape: BoxShape.circle,
                                      ),
                                      child: Container(
                                        width: 30,
                                        height: 30,
                                        alignment: Alignment.center,
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.orange,
                                        ),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ])),
                      )
                  ],
                )
              ])
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.grey,
        currentIndex: _selectedPageIndex,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.filter),
            label: 'Filter',
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.sort),
            label: 'Sort',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
