import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/introScreen.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';

class OtpVerifyScreen extends StatefulWidget {
  @override
  _OtpVerifyScreenState createState() => _OtpVerifyScreenState();
}

class _OtpVerifyScreenState extends State<OtpVerifyScreen> {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(deviceSize.height / 4 / 3),
        child: AppBar(
          backgroundColor: Color(0xFFF0F2F5),
          elevation: 1,
          automaticallyImplyLeading: false,
          title: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
            ),
            color: Colors.green[700],
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: [
            Container(
              margin: EdgeInsets.only(right: 20),
              child: Icon(
                Icons.menu,
                color: Color(0xFF212529),
              ),
            )
          ],
        ),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(top: deviceSize.height / 4 / 3),
                child: Column(
                  children: [
                    FittedBox(
                      child: Text(
                        'Verify your number',
                        style: TextStyle(
                            color: Color(0xFF212529),
                            fontWeight: FontWeight.w500,
                            fontSize: 30),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Enter the 4-digit code we sent to you',
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      '985638202',
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: deviceSize.height / 4 / 4),
                      child: PinEntryTextField(
                        showFieldAsBox: false,
                        onSubmit: (String pin) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("Pin"),
                                  content: Text('Pin entered is $pin'),
                                );
                              });
                        },
                      ),
                    ),
                    SizedBox(
                      height: deviceSize.height / 4 / 4,
                    ),
                    GestureDetector(
                      child: Text(
                        'Resend Code',
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.green),
                      ),
                    ),
                    SizedBox(
                      height: deviceSize.height / 4 / 4,
                    ),
                    GestureDetector(
                      child: Text(
                        'Call me instead',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                child: Card(
                  elevation: 0,
                  margin: EdgeInsets.only(left: 5, right: 5, bottom: 1),
                  child: Container(
                    width: double.infinity,
                    height: deviceSize.height / 4 / 2 * .7,
                    alignment: Alignment.center,
                    child: Text(
                      'Continue',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    decoration: BoxDecoration(
                      color: Color(0xFF19e81c),
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                    return IntroScreen();
                  }));
                },
              )
            ],
          ),
        ),
      ),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
        child: FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed: () {},
          child: FaIcon(
            FontAwesomeIcons.solidMoon,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
