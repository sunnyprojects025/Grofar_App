import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FilterScreen extends StatefulWidget {
  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  bool choosedvalue = false;
  double _value = 0;
  List sortby = [
    {'Answer': 'Top Rated', 'selected': false},
    {'Answer': 'Nearest Me', 'selected': false},
    {'Answer': 'Cost High to Low', 'selected': false},
    {'Answer': 'Cost Low to High', 'selected': false},
    {'Answer': 'Most Popular', 'selected': false},
  ];
  List filter = [
    {'Answer': 'Open Now', 'selected': false},
    {'Answer': 'Credit Cards', 'selected': false},
    {'Answer': 'Alcohol Served', 'selected': false},
  ];

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Card(
                elevation: 0,
                child: Container(
                  width: double.infinity,
                  height: deviceSize.height / 4 / 3,
                  color: Color(0xFFF0F2F5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.only(left: 15),
                          child: Text(
                            'Filter',
                            style: TextStyle(
                                color: Color(0xFF212529),
                                fontWeight: FontWeight.w500,
                                fontSize: 20),
                          )),
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          padding: EdgeInsets.all(5),
                          child: IconButton(
                            icon: Icon(Icons.cancel),
                            onPressed: () {
                              Navigator.of(context).pop(true);
                            },
                          ))
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(15),
                child: Text(
                  'Sort By',
                  style: TextStyle(
                      color: Color(0xFF212529),
                      fontWeight: FontWeight.w500,
                      fontSize: 20),
                ),
              ),
              Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    for (int index1 = 0; index1 < sortby.length; index1++)
                      GestureDetector(
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  sortby[index1]['Answer'],
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18),
                                ),
                              ),
                              Container(
                                width: 30,
                                height: 30,
                                child: sortby[index1]['selected'] == true
                                    ? Container(
                                        margin: EdgeInsets.all(8),
                                        width: 5,
                                        height: 5,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                        ),
                                      )
                                    : null,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: sortby[index1]['selected'] == true
                                        ? Colors.green
                                        : Colors.white,
                                    border: Border.all(color: Colors.grey)),
                              )
                            ],
                          ),
                        ),
                        onTap: () => setState(() {
                          sortby[index1]['selected'] =
                              !sortby[index1]['selected'];
                          choosedvalue = !choosedvalue;
                        }),
                      )
                  ]),
              Container(
                padding: EdgeInsets.all(15),
                child: Text(
                  'Filter',
                  style: TextStyle(
                      color: Color(0xFF212529),
                      fontWeight: FontWeight.w500,
                      fontSize: 20),
                ),
              ),
              Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    for (int index1 = 0; index1 < filter.length; index1++)
                      GestureDetector(
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text(
                                  filter[index1]['Answer'],
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18),
                                ),
                              ),
                              Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: filter[index1]['selected'] == true
                                        ? Colors.green
                                        : Colors.white,
                                    border: Border.all(color: Colors.grey)),
                              )
                            ],
                          ),
                        ),
                        onTap: () => setState(() {
                          filter[index1]['selected'] =
                              !filter[index1]['selected'];
                          choosedvalue = !choosedvalue;
                        }),
                      )
                  ]),
              Container(
                padding: EdgeInsets.all(15),
                child: Text(
                  'Additional Filters',
                  style: TextStyle(
                      color: Color(0xFF212529),
                      fontWeight: FontWeight.w500,
                      fontSize: 20),
                ),
              ),
              SliderTheme(
                data: SliderThemeData(
                    thumbColor: Colors.blue,
                    inactiveTrackColor: Colors.grey,
                    activeTrackColor: Colors.grey,
                    showValueIndicator: ShowValueIndicator.always,
                    thumbShape: RoundSliderThumbShape(enabledThumbRadius: 10)),
                child: Slider(
                  min: 0.0,
                  max: 220.0,
                  value: _value,
                  onChanged: (val) {
                    _value = val;
                    setState(() {});
                  },
                ),
              ),
              Container(
                  margin: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Price Range',
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w500,
                            fontSize: 18),
                      ),
                      SizedBox(
                        height: deviceSize.height / 4 / 4 / 2,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            child: Column(
                              children: [
                                Text(
                                  'Min',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18),
                                ),
                                SizedBox(
                                  height: deviceSize.height / 4 / 4 / 2,
                                ),
                                Text(
                                  '0',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    decoration: TextDecoration.underline,
                                    decorationThickness: 2,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: deviceSize.height / 4 / 4,
                                right: deviceSize.width / 4),
                            child: Text(_value.toStringAsFixed(2),
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    decoration: TextDecoration.underline,
                                    decorationThickness: 2)),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ))
            ]),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        // onTap: ,
        selectedItemColor: Colors.grey,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Colors.grey,
            icon: Icon(
              Icons.close,
            ),
            label: 'Close',
          ),
          BottomNavigationBarItem(
            backgroundColor: choosedvalue == true ? Colors.green : Colors.grey,
            icon: Icon(Icons.filter_alt_sharp),
            label: 'Apply',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
