import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/Drawers/Drawer.dart';

class PickTodayScreen extends StatelessWidget {
  List tiles = [
    {'image': 'assets/img/green/g1.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g2.jpg', 'item': 'Chili'},
    {'image': 'assets/img/green/g3.jpg', 'item': 'Onion'},
    {'image': 'assets/img/green/g4.jpg', 'item': 'Cabbage'},
    {'image': 'assets/img/green/g5.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g6.jpg', 'item': 'Jamun'},
    {'image': 'assets/img/green/g7.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g8.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g9.jpg', 'item': 'Friuts'},
  ];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      drawer: Drawer(
        child: DrawerScreen(),
      ),
      key: _scaffoldKey,
      body: SafeArea(
          child: Container(
        width: double.infinity,
        height: double.infinity,
        child: ListView(
          children: [
            Card(
              elevation: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.all(5),
                      child: FlatButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                            size: 20,
                          ),
                          label: Text(
                            'Pick\'s Today',
                            style: TextStyle(fontSize: 20),
                          ))),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      margin: EdgeInsets.only(top: 0, right: 10),
                      child: IconButton(
                        icon: Icon(
                          Icons.menu,
                          size: 30,
                          color: Color(0xFF212529),
                        ),
                        onPressed: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            GridView(
              scrollDirection: Axis.vertical,
              physics: BouncingScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 4 / 4.5,
                  crossAxisSpacing: 0,
                  mainAxisSpacing: 0),
              shrinkWrap: true,
              children: <Widget>[
                for (int index1 = 0; index1 < tiles.length; index1++)
                  Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Container(
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.only(top: 5),
                        child: Column(children: [
                          Container(
                            width: deviceSize.width / 2 * .6,
                            height: deviceSize.height / 4 * .5,
                            child: Stack(
                              children: [
                                Positioned(
                                    top: 10,
                                    left: 5,
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.orangeAccent[100],
                                      ),
                                      child: Text(
                                        "25%",
                                        style: TextStyle(
                                            color: Colors.orange[700]),
                                      ),
                                    )),
                              ],
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage(tiles[index1]['image']),
                                    fit: BoxFit.fill)),
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(5),
                                  margin: EdgeInsets.only(left: 10, top: 10),
                                  child: Text(
                                    tiles[index1]['item'],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xFF212529),
                                        fontSize: 16),
                                  ),
                                ),
                              ]),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  '\$ 1/kg',
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                              GestureDetector(
                                child: Container(
                                  margin: EdgeInsets.only(right: 10),
                                  width: 40,
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFF0F2F5),
                                    shape: BoxShape.circle,
                                  ),
                                  child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.add,
                                      color: Colors.orange,
                                    ),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )
                        ])),
                  )
              ],
            )
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
