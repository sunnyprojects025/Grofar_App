import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/signupScreen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: Color(0xFFF0F2F5),
        body: SafeArea(
            child: Column(
          children: [
            Expanded(
                child: ListView(children: [
              Card(
                elevation: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                            size: 20,
                          ),
                        )),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Container(
                        margin: EdgeInsets.only(top: 0, right: 10),
                        child: IconButton(
                          icon: Icon(
                            Icons.menu,
                            size: 30,
                            color: Color(0xFF212529),
                          ),
                          onPressed: () {
                            //_scaffoldKey.currentState.openDrawer();
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(top: 20, left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FittedBox(
                      child: Text(
                        'Welcome Back',
                        style: TextStyle(
                            color: Color(0xFF212529),
                            fontWeight: FontWeight.w500,
                            fontSize: 30),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Sign in to Continue',
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              Form(
                  child: Column(children: [
                Container(
                  margin: EdgeInsets.only(left: 10, top: 10),
                  child: Form(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Text(
                            'Email',
                            style: TextStyle(fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: TextFormField(
                              decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            hintText: 'Enter Email',
                            hintStyle: TextStyle(color: Colors.grey),
                          )),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Text(
                            'Password ',
                            style: TextStyle(fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: TextFormField(
                              decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            hintText: 'Enter Password',
                            hintStyle: TextStyle(color: Colors.grey),
                          )),
                        ),
                        GestureDetector(
                          child: Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            margin: EdgeInsets.only(
                                left: 5,
                                right: 10,
                                bottom: deviceSize.height / 4 / 4 / 2,
                                top: 10),
                            child: Container(
                              width: double.infinity,
                              height: deviceSize.height / 4 / 2 * .7,
                              alignment: Alignment.center,
                              child: Text(
                                'Sign in',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              decoration: BoxDecoration(
                                  color: Color(0xFF19e81c),
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                          ),
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (ctx) {
                              return SignUpScreen();
                            }));
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ]))
            ])),
            GestureDetector(
              child: Container(
                width: double.infinity,
                height: deviceSize.height / 4 / 2 * .7,
                margin: EdgeInsets.only(bottom: 5),
                alignment: Alignment.center,
                child: Text(
                  'Don\'t have a account signup?',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5)),
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                  return SignUpScreen();
                }));
              },
            ),
          ],
        )),
        floatingActionButton: Container(
          margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
          child: FloatingActionButton(
            backgroundColor: Colors.black,
            onPressed: () {},
            child: FaIcon(
              FontAwesomeIcons.solidMoon,
              color: Colors.white,
            ),
          ),
        ));
  }
}
