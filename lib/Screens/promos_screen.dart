import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PromosScreen extends StatelessWidget {
  final List promos = [
    {
      'title': 'Grocery',
      'subtitle': 'Banana 25% off',
      'status': 'check now',
      'color': Colors.green,
      'image': 'assets/img/promos/p1.png'
    },
    {
      'title': 'Grocery',
      'subtitle': 'Banana 25% off',
      'status': 'check now',
      'color': Colors.blue,
      'image': 'assets/img/promos/p2.png'
    },
    {
      'title': 'Grocery',
      'subtitle': 'Banana 25% off',
      'status': 'check now',
      'color': Colors.orange,
      'image': 'assets/img/promos/p3.png'
    },
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
          child: Container(
        width: double.infinity,
        height: double.infinity,
        child: ListView(
          children: [
            Card(
              elevation: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.all(5),
                      child: FlatButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                            size: 20,
                          ),
                          label: Text(
                            'Promos',
                            style: TextStyle(fontSize: 20),
                          ))),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      margin: EdgeInsets.only(top: 6, right: 10),
                      child: Icon(
                        Icons.menu,
                        size: 30,
                        color: Color(0xFF212529),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15, left: 15, bottom: 15),
              child: Text(
                'Available Promos',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 23,
                    fontWeight: FontWeight.bold),
              ),
            ),
            for (int index1 = 0; index1 < promos.length; index1++)
              Card(
                elevation: 3,
                color: promos[index1]['color'],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                margin:
                    EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 15),
                child: Container(
                  padding:
                      EdgeInsets.only(left: 10, right: 10, bottom: 15, top: 15),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('assets/logos/logo.png'),
                                        fit: BoxFit.fill)),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                  width: deviceSize.width / 4 * .8,
                                  child: FittedBox(
                                      child: Text(
                                    (promos[index1]['title']),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  )))
                            ],
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                (promos[index1]['subtitle']),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )),
                          Container(
                            width: deviceSize.width / 3 * .8,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: Colors.white,
                                )),
                            child: FittedBox(
                              child: FlatButton.icon(
                                  onPressed: () {},
                                  icon: FaIcon(
                                    FontAwesomeIcons.percentage,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    'Check now',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ),
                          )
                        ],
                      ),
                      Container(
                        width: deviceSize.width / 4 * .7,
                        height: deviceSize.height / 4 / 3,
                        margin: EdgeInsets.only(right: 15),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                                image: AssetImage(promos[index1]['image']),
                                fit: BoxFit.fill)),
                      ),
                    ],
                  ),
                ),
              )
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
