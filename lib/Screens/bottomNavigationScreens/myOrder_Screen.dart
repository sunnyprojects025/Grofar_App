import 'package:flutter/material.dart';
import 'package:grofar/Screens/Drawers/myOrder_drawer.dart';
import 'package:grofar/Widgets/cancelled_order_widget.dart';
import 'package:grofar/Widgets/completed_order_widget.dart';
import 'package:grofar/Widgets/progress_order_widget.dart';

class MyOrderScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          key: _scaffoldKey,
          drawer: Drawer(
            child: MyOrderDrawer(),
          ),
          backgroundColor: Color(0xFFF0F2F5),
          appBar: AppBar(
            backgroundColor: Color(0xFFF0F2F5),
            automaticallyImplyLeading: false,
            bottom: TabBar(
              tabs: [
                Tab(
                  child: Text('Completed'),
                ),
                Tab(
                  child: Text('On Progress'),
                ),
                Tab(
                  child: Text('Cancelled'),
                ),
              ],
              indicatorColor: Colors.green,
              labelColor: Colors.green,
              physics: BouncingScrollPhysics(),
              unselectedLabelColor: Colors.grey[700],
            ),
            title: Container(
                width: double.infinity,
                margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 4 / 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'My Order',
                      style: TextStyle(color: Colors.black),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.menu,
                        size: 30,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        _scaffoldKey.currentState.openDrawer();
                      },
                    )
                  ],
                )),
          ),
          body: TabBarView(
            children: [
              CompletedOrderWidget(),
              ProgressOrderWidget(),
              CancelledOrderWidget()
            ],
          ),
        ),
      ),
    );
  }
}
