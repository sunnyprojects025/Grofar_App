import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  int counter = 1;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [
              Card(
                elevation: 0,
                child: Container(
                  width: double.infinity,
                  height: deviceSize.height / 4 / 3,
                  color: Color(0xFFF0F2F5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.only(left: 15),
                          child: Text(
                            'Cart',
                            style: TextStyle(
                                color: Color(0xFF212529),
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          )),
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          padding: EdgeInsets.all(5),
                          child: IconButton(
                            icon: Icon(
                              Icons.menu,
                              size: 30,
                            ),
                            onPressed: () {},
                          ))
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: SingleChildScrollView(
                      child: Column(children: [
                Card(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 15, bottom: 15),
                      child: Column(children: [
                        ListTile(
                          leading: Container(
                            width: deviceSize.width / 4 * .8,
                            height: deviceSize.height / 4,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                    image:
                                        AssetImage('assets/img/green/g1.jpg'),
                                    fit: BoxFit.fill)),
                          ),
                          title: Wrap(
                            direction: Axis.vertical,
                            children: [
                              Text(
                                'Spinach',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18),
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text(
                                '0.82/kg',
                                style: TextStyle(
                                    decoration: TextDecoration.lineThrough,
                                    color: Colors.green,
                                    decorationColor: Colors.green,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                  top: deviceSize.height / 4 / 4 * .2,
                                  left: deviceSize.width / 4 * .9),
                              child: Text(
                                '\$ 2.82',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: deviceSize.width / 4 / 2),
                              padding: EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.grey.withOpacity(.2)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  GestureDetector(
                                    child: Container(
                                      width: 30,
                                      height: 30,
                                      margin: EdgeInsets.only(right: 10),
                                      alignment: Alignment.center,
                                      child: Icon(
                                        Icons.remove,
                                        color: Colors.orange,
                                      ),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        counter == 0 ? null : counter--;
                                      });
                                    },
                                  ),
                                  Container(
                                    child: Text(counter.toString()),
                                  ),
                                  GestureDetector(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      width: 30,
                                      height: 30,
                                      alignment: Alignment.center,
                                      child: Icon(
                                        Icons.add,
                                        color: Colors.orange,
                                      ),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        counter++;
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                          ],
                        )
                      ]),
                    )),
              ]))),
              GestureDetector(
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  margin: EdgeInsets.only(
                      left: 5,
                      right: 5,
                      bottom: deviceSize.height / 4 / 3,
                      top: 10),
                  child: Container(
                    width: double.infinity,
                    height: deviceSize.height / 4 / 2 * .7,
                    child: ListTile(
                      leading: Wrap(
                        direction: Axis.vertical,
                        children: [
                          Text(
                            'Subtotal \$ 8.52',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 17),
                          ),
                          Text(
                            'Proceed To Checkout',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      trailing: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                      ),
                    ),
                    decoration: BoxDecoration(
                        color: Color(0xFF39A94B),
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ),
                onTap: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
