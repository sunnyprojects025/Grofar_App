import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/DrawerItemScreens/editProfile_screen.dart';
import 'package:grofar/Screens/Drawers/account_drawer.dart';
import 'package:grofar/Screens/DrawerItemScreens/help&Support_screen.dart';
import 'package:grofar/Screens/login_screen.dart';
import 'package:grofar/Screens/promos_page_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/selectAddress_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/termsConditions_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/DefaulScreen.dart';

class AccountScreen extends StatelessWidget {
  final List accountDetails = [
    {
      'icon': Container(
        width: 30,
        height: 30,
        child: FloatingActionButton(
          heroTag: 'first_btn',
          backgroundColor: Colors.green,
          onPressed: () {},
          child: Icon(Icons.star, color: Colors.white),
        ),
      ),
      'title': 'Promos',
      'widget': PromosPageScreen()
    },
    {
      'icon': Container(
        width: 30,
        height: 30,
        child: FloatingActionButton(
          heroTag: 'second_btn',
          backgroundColor: Colors.black,
          onPressed: () {},
          child: FaIcon(FontAwesomeIcons.addressCard, color: Colors.white),
        ),
      ),
      'title': 'My Address',
      'widget': SelectAddressScreen()
    },
    {
      'icon': Container(
        width: 30,
        height: 30,
        child: FloatingActionButton(
          heroTag: 'third_btn',
          backgroundColor: Colors.blue,
          onPressed: () {},
          child: Icon(Icons.error, color: Colors.white),
        ),
      ),
      'title': 'Terms, Privacy & Policy',
      'widget': TermsConditionsScreen()
    },
    {
      'icon': Container(
        width: 30,
        height: 30,
        child: FloatingActionButton(
          heroTag: 'fourth_btn',
          backgroundColor: Colors.yellow,
          onPressed: () {},
          child: Icon(Icons.phone, color: Colors.white),
        ),
      ),
      'title': 'Help & Support',
      'widget': HelpSupportScreen()
    },
    {
      'icon': Container(
        width: 30,
        height: 30,
        child: FloatingActionButton(
          heroTag: 'fifth_btn',
          backgroundColor: Colors.orange,
          onPressed: () {},
          child: Icon(Icons.lock, color: Colors.white),
        ),
      ),
      'title': 'Logout',
      'widget': LoginScreen()
    },
  ];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: AccountDrawer(),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: deviceSize.height / 2 * .9,
              child: Column(
                children: [
                  Card(
                    elevation: 2,
                    child: Container(
                      width: double.infinity,
                      height: deviceSize.height / 4 / 3,
                      color: Color(0xFFF0F2F5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.only(left: 15),
                              child: Text(
                                'My Account',
                                style: TextStyle(
                                    color: Color(0xFF212529),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              )),
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              padding: EdgeInsets.all(5),
                              child: IconButton(
                                icon: Icon(
                                  Icons.menu,
                                  size: 30,
                                ),
                                onPressed: () {
                                  _scaffoldKey.currentState.openDrawer();
                                },
                              ))
                        ],
                      ),
                    ),
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              top: deviceSize.height / 4 / 4 / 2),
                          child: CircleAvatar(
                            radius: 50,
                            backgroundImage: AssetImage(
                              'assets/img/user.jpg',
                            ),
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'Askbootstrap',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 19),
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text('iamsohan@gmail.com')
                            ],
                          ),
                        ),
                        GestureDetector(
                          child: Container(
                            padding: EdgeInsets.all(8),
                            margin: EdgeInsets.only(
                                top: deviceSize.height / 4 / 4 / 2),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.green,
                            ),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                ),
                                Text(
                                  'Edit Profile',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (ctx) {
                              return EditProfileScreen();
                            }));
                          },
                        )
                      ],
                    ),
                  ]),
                ],
              ),
            ),
            Expanded(
                child: SingleChildScrollView(
                    child: Column(
              children: [
                for (int index1 = 0; index1 < accountDetails.length; index1++)
                  GestureDetector(
                      child: Card(
                        elevation: 0,
                        child: Container(
                          padding: EdgeInsets.only(
                              left: 10, right: 10, top: 20, bottom: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    accountDetails[index1]['icon'],
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(accountDetails[index1]['title'])
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 20),
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.green.withOpacity(.3),
                                ),
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  size: 15,
                                  color: Colors.green,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (ctx) {
                          return accountDetails[index1]['widget'] == null
                              ? null
                              : accountDetails[index1]['widget'];
                        }));
                      })
              ],
            )))
          ],
        ),
      ),
    );
  }
}
