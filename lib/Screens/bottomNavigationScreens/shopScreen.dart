import 'package:flutter/material.dart';
import 'package:grofar/Screens/DrawerItemScreens/notification_screen.dart';
import 'package:grofar/Screens/Drawers/Drawer.dart';
import 'package:grofar/Screens/search_screen.dart';
import '../../Widgets/lookingforCard_Widget.dart';
import '../../Widgets/pickToday_widget.dart';
import '../../Widgets/promosforYou_widget.dart';
import '../../Widgets/recommendedForyou_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: DrawerScreen(),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: ListView(
          shrinkWrap: true,
          children: [
            Card(
              color: Color(0xFFF0F2F5),
              elevation: 1,
              child: Container(
                width: deviceSize.width,
                height: deviceSize.height / 4 * .8,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Wrap(
                            direction: Axis.horizontal,
                            children: [
                              Container(
                                width: 50,
                                height: 50,
                                child: Image.asset(
                                  'assets/logos/logo.png',
                                  fit: BoxFit.fill,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8),
                                child: FittedBox(
                                  child: Text(
                                    'Grocery',
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 30),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Wrap(
                            direction: Axis.horizontal,
                            children: [
                              GestureDetector(
                                child: Card(
                                  elevation: 3,
                                  child: Container(
                                      padding: EdgeInsets.all(5),
                                      child: Row(
                                        children: [
                                          Icon(Icons.notification_important),
                                          Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                color: Colors.orangeAccent[100],
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Text(
                                              '2',
                                              style: TextStyle(
                                                  color: Colors.orange[700],
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          )
                                        ],
                                      )),
                                ),
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (ctx) {
                                    return NotifiactionScreen();
                                  }));
                                },
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                child: IconButton(
                                  icon: Icon(
                                    Icons.menu,
                                    color: Color(0xFF212529),
                                  ),
                                  onPressed: () {
                                    _scaffoldKey.currentState.openDrawer();
                                  },
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    GestureDetector(
                      child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          alignment: Alignment.center,
                          width: double.infinity,
                          height: deviceSize.height / 4 / 4,
                          child: TextFormField(
                            keyboardType: TextInputType.multiline,
                            maxLines: 2,
                            decoration: InputDecoration(
                                enabled: false,
                                border: InputBorder.none,
                                hintText: 'Search for products..',
                                prefixIcon: Icon(Icons.search)),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (ctx) {
                          return SearchScreen();
                        }));
                      },
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  left: deviceSize.width / 4 / 3, top: 15, bottom: 15),
              child: Text(
                'What do you Looking For?',
                style: TextStyle(
                    color: Color(0xFF212529),
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    letterSpacing: .8),
              ),
            ),
            LookingForCard(),
            PromosForYou(),
            PickToday(),
            RecommendedForYou()
          ],
        ),
      ),
    );
  }
}
