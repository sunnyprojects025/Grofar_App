import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List searchItem = [
    {
      'title': 'Fresh Vegetable',
      'subtitle': 'Fresh Premium Vegetable Item form Thailand',
      'image': Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
            image:
                DecorationImage(image: AssetImage('assets/img/cart/g1.png'))),
      )
    },
    {
      'title': 'Fresh Vegetable',
      'subtitle': 'Fresh Premium Vegetable Item form Thailand',
      'image': Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/img/cart/g1.png'), fit: BoxFit.fill)),
      )
    },
    {
      'title': 'Fresh Vegetable',
      'subtitle': 'Fresh Premium Vegetable Item form Thailand',
      'image': Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
            image:
                DecorationImage(image: AssetImage('assets/img/cart/g1.png'))),
      )
    },
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFF0F2F5),
      body: SafeArea(
          child: Container(
        width: double.infinity,
        height: double.infinity,
        child: ListView(
          children: [
            Card(
              elevation: 1,
              child: Container(
                width: double.infinity,
                height: deviceSize.height / 4 / 2 * .9,
                color: Color(0xFFF0F2F5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    Expanded(
                      child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          alignment: Alignment.center,
                          height: deviceSize.height / 4 / 4,
                          child: TextFormField(
                            keyboardType: TextInputType.multiline,
                            maxLines: 2,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Search for products..',
                                prefixIcon: Icon(Icons.search)),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            for (int index1 = 0; index1 < searchItem.length; index1++)
              Card(
                child: Container(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: ListTile(
                    leading: searchItem[index1]['image'],
                    title: Text(
                      searchItem[index1]['title'],
                    ),
                    subtitle: Text(
                      searchItem[index1]['subtitle'],
                    ),
                  ),
                ),
              )
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
