import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'bottomNavigationScreens/accountScreen.dart';
import 'bottomNavigationScreens/cartScreen.dart';
import 'bottomNavigationScreens/myOrder_Screen.dart';
import 'bottomNavigationScreens/shopScreen.dart';

class BottomNavigationScreen extends StatefulWidget {
  @override
  BottomNavigationScreenState createState() => BottomNavigationScreenState();
}

class BottomNavigationScreenState extends State<BottomNavigationScreen> {
  int _index = 0;

  bool selectedText = false;

  List _bottomnavigation = [
    {
      'icon': Icon(
        Icons.store,
      ),
      'label': 'Shop',
    },
    {
      'icon': Icon(
        Icons.shopping_cart,
      ),
      'label': 'Cart',
    },
    {
      'icon': Icon(
        Icons.shopping_bag,
      ),
      'label': 'My Order',
    },
    {
      'icon': Icon(
        Icons.supervised_user_circle,
      ),
      'label': 'Account',
    },
  ];
  @override
  void initState() {
    super.initState();
    _index = 0;
  }

  @override
  Widget build(BuildContext context) {
    Widget child;
    switch (_index) {
      case 0:
        child = HomeScreen();
        break;
      case 1:
        child = CartScreen();
        break;
      case 2:
        child = MyOrderScreen();
        break;
      case 3:
        child = AccountScreen();
        break;
    }
    return Scaffold(
      backgroundColor: Color(0xFFF0F2F5),
      body: SafeArea(child: child),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 2,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.grey,
        type: BottomNavigationBarType.fixed,
        onTap: (newIndex) => setState(() => _index = newIndex),
        currentIndex: _index,
        items: [
          ..._bottomnavigation.map(
            (bottomitem) => BottomNavigationBarItem(
              icon: bottomitem['icon'],
              backgroundColor:
                  selectedText == false ? Colors.grey : Colors.green,
              label: bottomitem['label'],
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
