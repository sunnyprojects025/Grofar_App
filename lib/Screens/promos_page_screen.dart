import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PromosPageScreen extends StatelessWidget {
  final String image;

  const PromosPageScreen({this.image});
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                Card(
                  elevation: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(5),
                          child: IconButton(
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.green,
                              size: 20,
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          margin: EdgeInsets.only(top: 6, right: 10),
                          child: Icon(
                            Icons.menu,
                            size: 30,
                            color: Color(0xFF212529),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: deviceSize.height / 4 * .8,
                  padding: EdgeInsets.only(top: 10),
                  color: Colors.green,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                            child: Wrap(
                              direction: Axis.horizontal,
                              children: [
                                Container(
                                  width: 35,
                                  height: 35,
                                  child: Image.asset(
                                    'assets/logos/logo.png',
                                    fit: BoxFit.fill,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 3),
                                  child: FittedBox(
                                    child: Text(
                                      'Grocery',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 25),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.white)),
                              child: FlatButton.icon(
                                  onPressed: () {},
                                  icon: FaIcon(
                                    FontAwesomeIcons.tag,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    'Osanch Off',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  )))
                        ],
                      ),
                      Container(
                        width: deviceSize.width / 4,
                        height: deviceSize.height / 4 / 2,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                                image: AssetImage(image == null
                                    ? 'assets/logos/logo.png'
                                    : image),
                                fit: BoxFit.fill)),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(top: 20, left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FittedBox(
                        child: Text(
                          'Get 25% off buying',
                          style: TextStyle(
                              color: Colors.green,
                              fontWeight: FontWeight.w500,
                              fontSize: 30),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Available Until 24 july 2021',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(top: 10, left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FittedBox(
                        child: Text(
                          'Highlights',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Enjoy free delivery every buy long bread, only at New York. Thank You for choosing Grocery 😊',
                        style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 15,
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(top: 10, left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FittedBox(
                        child: Text(
                          'Terms \$ Conditions',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Easypromos only uses detials of its users to facilitate the successful operation of the promotions.Easypromos will never utilize detials of registered users for any other reason.Easypromos cannot be held responsible for the prizes, products or services offered to useres thorugh the promotions.Easypromos only uses detials of its users to facilitate the successful operation of the promotions.Easypromos will never utilize detials of registered users for any other reason.Easypromos cannot be held responsible for the prizes, products or services offered to useres thorugh the promotions.',
                        style: TextStyle(
                            letterSpacing: .7,
                            color: Colors.grey[600],
                            fontSize: 15,
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              margin: EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 10),
              child: Container(
                width: double.infinity,
                height: deviceSize.height / 4 / 2 * .7,
                alignment: Alignment.center,
                child: Text(
                  'Buy now',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                decoration: BoxDecoration(
                    color: Color(0xFF19e81c),
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
            onTap: () {},
          ),
        ],
      )),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
        child: FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed: () {},
          child: FaIcon(
            FontAwesomeIcons.solidMoon,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
