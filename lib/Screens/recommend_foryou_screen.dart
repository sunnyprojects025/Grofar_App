import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RecommendForYouScreen extends StatelessWidget {
  final List image;

  RecommendForYouScreen({this.image});
  List icons = [
    Icon(Icons.star, color: Colors.orange),
    Icon(Icons.star, color: Colors.orange),
    Icon(Icons.star, color: Colors.orange),
    Icon(Icons.star, color: Colors.orange),
    Icon(Icons.star)
  ];

  List tiles = [
    {'image': 'assets/img/green/g1.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g2.jpg', 'item': 'Chili'},
    {'image': 'assets/img/green/g3.jpg', 'item': 'Onion'},
    {'image': 'assets/img/green/g4.jpg', 'item': 'Cabbage'},
    {'image': 'assets/img/green/g5.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g6.jpg', 'item': 'Jamun'},
    {'image': 'assets/img/green/g7.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g8.jpg', 'item': 'Friuts'},
    {'image': 'assets/img/green/g9.jpg', 'item': 'Friuts'},
  ];
  final List images = [
    'assets/img/recommend/r1.jpg',
    'assets/img/recommend/r2.jpg',
    'assets/img/recommend/r3.jpg',
  ];

  final List itempieces = ['4pcs', '6pcs', '8pcs'];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        //  backgroundColor: Color(0xFFF0F2F5),
        body: SafeArea(
            child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(5),
                          child: FlatButton.icon(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.green,
                                size: 20,
                              ),
                              label: Text(
                                'Back',
                                style: TextStyle(
                                    color: Colors.green, fontSize: 20),
                              ))),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Wrap(
                          direction: Axis.horizontal,
                          children: [
                            Container(
                              width: 40,
                              height: 40,
                              child: FloatingActionButton(
                                heroTag: 'first_button',
                                backgroundColor: Colors.orange,
                                onPressed: () {},
                                child: Icon(
                                  Icons.favorite,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              width: 40,
                              height: 40,
                              child: FloatingActionButton(
                                heroTag: 'second-button',
                                backgroundColor: Colors.green,
                                onPressed: () {},
                                child: Icon(
                                  Icons.share,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 6),
                              child: Icon(
                                Icons.menu,
                                size: 30,
                                color: Color(0xFF212529),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(10),
                    child: Text(
                      'Valencina Orange Imported',
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.all(10),
                        child: Text(
                          'Product Mrp: \$\ 263.00',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          '50% OFF',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.orange),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.orange)),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.all(10),
                          child: Row(
                            children: [
                              ...icons.map(
                                (icons) => icons,
                              )
                            ],
                          )),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          '(205 Reviews)',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Card(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: Container(
                      width: double.infinity,
                      height: deviceSize.height * .6 * .9,
                      padding: EdgeInsets.all(5),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Delivery',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                    Text(
                                      'Free',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(right: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Available in:',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                    Text(
                                      '1kg, 2kg, 3kg',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          Container(
                            width: double.infinity,
                            height: deviceSize.height / 2 * .6,
                            color: Color(0xFFF0F2F5),
                            margin: EdgeInsets.all(8),
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: [
                                ...images.map((item) => Container(
                                      width: deviceSize.width * .8,
                                      height: deviceSize.height / 3 * .8,
                                      margin: EdgeInsets.all(5),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Image.asset(
                                            item,
                                            fit: BoxFit.fill,
                                          )),
                                    ))
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Wrap(direction: Axis.horizontal, children: [
                                ...itempieces.map(
                                  (item) => Container(
                                    padding: EdgeInsets.all(5),
                                    margin: EdgeInsets.only(left: 15),
                                    child: Text(
                                      item,
                                      style: TextStyle(
                                          color: Colors.orange,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        border:
                                            Border.all(color: Colors.orange)),
                                  ),
                                )
                              ]),
                              GestureDetector(
                                child: Container(
                                  margin: EdgeInsets.only(right: 10),
                                  width: 40,
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFF0F2F5),
                                    shape: BoxShape.circle,
                                  ),
                                  child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.add,
                                      color: Colors.orange,
                                    ),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      height: deviceSize.height / 4 / 4,
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 2,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'type Your city...',
                            prefixIcon: Icon(Icons.search)),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(top: 10, left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FittedBox(
                          child: Text(
                            'Product Details',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 20),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Easypromos only uses detials of its users to facilitate the successful operation of the promotions.Easypromos will never utilize detials of registered users for any other reason, products or services offered to useres thorugh the promotions.',
                          style: TextStyle(
                              letterSpacing: .7,
                              color: Colors.grey[600],
                              fontSize: 15,
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(top: 10, left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FittedBox(
                          child: Text(
                            'Maybe You Like this',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 20),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Column(children: [
                          GridView(
                            scrollDirection: Axis.vertical,
                            physics: BouncingScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              childAspectRatio: 4 / 5.4,
                            ),
                            shrinkWrap: true,
                            children: <Widget>[
                              for (int index1 = 0;
                                  index1 < tiles.length;
                                  index1++)
                                Card(
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Container(
                                      padding: EdgeInsets.all(5),
                                      child: Column(children: [
                                        Container(
                                          width: deviceSize.width / 2 * .7,
                                          height: deviceSize.height / 4 * .7,
                                          child: Stack(
                                            children: [
                                              Positioned(
                                                  top: 10,
                                                  left: 5,
                                                  child: Container(
                                                    padding: EdgeInsets.all(5),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      color: Colors
                                                          .orangeAccent[100],
                                                    ),
                                                    child: Text(
                                                      "25%",
                                                      style: TextStyle(
                                                          color: Colors
                                                              .orange[700]),
                                                    ),
                                                  )),
                                            ],
                                          ),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      tiles[index1]['image']),
                                                  fit: BoxFit.fill)),
                                        ),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.all(5),
                                                margin: EdgeInsets.only(
                                                    left: 10, top: 10),
                                                child: Text(
                                                  tiles[index1]['item'],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Color(0xFF212529),
                                                      fontSize: 16),
                                                ),
                                              ),
                                            ]),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(10),
                                              child: Text(
                                                '\$ 1/kg',
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18),
                                              ),
                                            ),
                                            GestureDetector(
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(right: 10),
                                                width: 40,
                                                height: 40,
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                  color: Color(0xFFF0F2F5),
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Container(
                                                  width: 30,
                                                  height: 30,
                                                  alignment: Alignment.center,
                                                  child: Icon(
                                                    Icons.add,
                                                    color: Colors.orange,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    shape: BoxShape.circle,
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        )
                                      ])),
                                )
                            ],
                          )
                        ])
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Container(
                  width: deviceSize.width / 3 * .9,
                  height: deviceSize.height / 4 / 2 * .7,
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.shopping_cart,
                    color: Colors.black,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.amberAccent,
                  ),
                ),
                Expanded(
                  child: Container(
                    height: deviceSize.height / 4 / 2 * .7,
                    alignment: Alignment.center,
                    child: Text(
                      'Buy',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.green,
                    ),
                  ),
                ),
              ],
            )
          ],
        )),
        floatingActionButton: Container(
          margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
          child: FloatingActionButton(
            backgroundColor: Colors.black,
            onPressed: () {},
            child: FaIcon(
              FontAwesomeIcons.solidMoon,
              color: Colors.white,
            ),
          ),
        ));
  }
}
