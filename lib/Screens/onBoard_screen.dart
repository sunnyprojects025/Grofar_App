import 'package:flutter/material.dart';
import 'package:grofar/Screens/login_screen.dart';
import '../Widgets/videoplayer.dart';
import 'signupScreen.dart';

class OnBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: [
          BackgroundVideoplayer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: deviceSize.width / 4 * .7,
                    height: deviceSize.height / 4 / 2,
                    margin: EdgeInsets.all(20),
                    child: Image.asset('assets/logos/logo.png'),
                  )
                ],
              ),
              Container(
                width: deviceSize.width,
                height: deviceSize.height * .5,
                margin: EdgeInsets.only(left: 20, right: 20),
                padding:
                    EdgeInsets.only(bottom: deviceSize.height / 4 / 4 * .9),
                // color: Colors.amber,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        margin: EdgeInsets.only(
                            left: 5,
                            right: 5,
                            bottom: deviceSize.height / 4 / 4 / 2,
                            top: 10),
                        child: Container(
                          width: double.infinity,
                          height: deviceSize.height / 4 / 2 * .7,
                          alignment: Alignment.center,
                          child: Text(
                            'Sign up',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFF19e81c),
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (ctx) {
                          return SignUpScreen();
                        }));
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          child: Container(
                            width: deviceSize.width * .8,
                            margin: EdgeInsets.only(
                              left: 10,
                              right: 10,
                            ),
                            child: FittedBox(
                              child: Text(
                                'Already have an Account? Login Here',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (ctx) {
                              return LoginScreen();
                            }));
                          },
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
