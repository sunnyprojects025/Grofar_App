import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HelpSupportScreen extends StatelessWidget {
  final String title;

  final List helpSupport = [
    'Have to check my order status?',
    'change items in my order?',
    'cancel my order?',
    'changes my delivery address?',
    'help with a pick order?',
    'Refunding Payment'
  ];

  HelpSupportScreen({this.title});
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFF0F2F5),
      body: SafeArea(
        child: ListView(
          children: [
            Card(
              color: Colors.white,
              elevation: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      //  color: Colors.white,
                      padding: EdgeInsets.all(5),
                      child: FlatButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                            size: 20,
                          ),
                          label: Text(
                            title == null ? 'Help & Support' : title,
                            style: TextStyle(fontSize: 20),
                          ))),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      margin: EdgeInsets.only(top: 0, right: 8),
                      child: IconButton(
                        icon: Icon(
                          Icons.menu,
                          size: 30,
                          color: Color(0xFF212529),
                        ),
                        onPressed: () {
                          // _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            for (int index1 = 0; index1 < helpSupport.length; index1++)
              Card(
                elevation: 2,
                margin: EdgeInsets.only(
                  left: 5,
                  right: 5,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 15, bottom: 15),
                      child: Text(helpSupport[index1],
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.grey,
                              fontSize: 16)),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.green,
                        size: 15,
                      ),
                    )
                  ],
                ),
              )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
