import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DeactivateAccountScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
          child: ListView(
        children: [
          Card(
            color: Color(0xFFF0F2F5),
            elevation: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    //  color: Colors.white,
                    padding: EdgeInsets.all(5),
                    child: FlatButton.icon(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.green,
                          size: 20,
                        ),
                        label: Text(
                          'Deactivate Account',
                          style: TextStyle(fontSize: 20),
                        ))),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    margin: EdgeInsets.only(top: 0, right: 8),
                    child: IconButton(
                      icon: Icon(
                        Icons.menu,
                        size: 30,
                        color: Color(0xFF212529),
                      ),
                      onPressed: () {
                        // _scaffoldKey.currentState.openDrawer();
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
          Form(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15, top: 10),
                child: Text(
                  'Full Name',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black),
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                child: Text(
                  'i am john@gmial.com',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, top: 10),
                child: Text(
                  'Number',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black),
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                child: Text(
                  '12247244',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, top: 10),
                child: Text(
                  'Paswword',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: TextFormField(
                    decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(left: 15),
                  hintText: 'Enter Password',
                  hintStyle: TextStyle(color: Colors.grey),
                )),
              ),
              GestureDetector(
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  margin:
                      EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 10),
                  child: Container(
                    width: double.infinity,
                    height: deviceSize.height / 4 / 2 * .7,
                    alignment: Alignment.center,
                    child: Text(
                      'CONFIRM DEACTIVATE',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    decoration: BoxDecoration(
                        color: Color(0xFF19e81c),
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ),
                onTap: () {},
              ),
            ],
          )),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Container(
              margin: EdgeInsets.only(left: 15, top: 20),
              child: Text(
                'When you deactivate the account',
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
              ),
            ),
            Container(
              margin: EdgeInsets.all(15),
              child: Text(
                'We will miss you Coming back is easy We will miss you Coming back is easyWe will miss you Coming back is easy We will miss you Coming back is easyWe will miss you Coming back is easyWe will miss you Coming back is easyWe will miss you Coming back is easyWe will miss you Coming back is easyWe will miss you Coming back is easy',
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.grey),
              ),
            ),
          ])
        ],
      )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
