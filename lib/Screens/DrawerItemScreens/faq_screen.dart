import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FAQScreen extends StatelessWidget {
  final List faqQues = [
    'Do you have Any built in caching?',
    'can i add/ upgradge my plan at a time?',
    'ehat acess comes in hosting place?',
    'how do i change my password?'
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Card(
              elevation: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.all(5),
                      child: FlatButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                            size: 20,
                          ),
                          label: Text(
                            'FAQ',
                            style: TextStyle(fontSize: 20),
                          ))),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      margin: EdgeInsets.only(top: 0, right: 8),
                      child: IconButton(
                        icon: Icon(
                          Icons.menu,
                          size: 30,
                          color: Color(0xFF212529),
                        ),
                        onPressed: () {
                          // _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: deviceSize.height / 4 / 4 / 2,
                  left: deviceSize.width / 4 / 4),
              child: Text(
                'Basics',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            SizedBox(height: deviceSize.height / 4 / 4 / 2),
            for (int index1 = 0; index1 < faqQues.length; index1++)
              Card(
                elevation: 2,
                margin: EdgeInsets.only(
                  left: 5,
                  right: 5,
                ),
                child: Container(
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  padding:
                      EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
                  child: Text(faqQues[index1],
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.grey,
                          fontSize: 16)),
                ),
              )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
