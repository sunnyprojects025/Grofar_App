import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/DrawerItemScreens/paymethod_screen.dart';

class ChangeDeliveryAddressScreen extends StatefulWidget {
  @override
  _ChangeDeliveryAddressScreenState createState() =>
      _ChangeDeliveryAddressScreenState();
}

class _ChangeDeliveryAddressScreenState
    extends State<ChangeDeliveryAddressScreen> {
  final List dates = [
    {'day': 'MON', 'date': '7 Sep', 'selected': true},
    {'day': 'TUE', 'date': '8 Sep', 'selected': false},
    {'day': 'WED', 'date': '9 Sep', 'selected': false},
    {'day': 'THUR', 'date': '10 Sep', 'selected': false},
    {'day': 'FRI', 'date': '11 Sep', 'selected': false},
    {'day': 'SAT', 'date': '12 Sep', 'selected': false}
  ];
  bool choosedvalue = false;

  List sortby = [
    {'Answer': '6AM-10AM', 'selected': false},
    {'Answer': '4PM-6AM', 'selected': false},
    {'Answer': '6PM-9PM', 'selected': false},
    {'Answer': '10AM-1PM', 'selected': false},
  ];

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFF0F2F5),
      body: SafeArea(
          child: Column(
        children: [
          Card(
            elevation: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    padding: EdgeInsets.all(5),
                    child: FlatButton.icon(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.green,
                          size: 20,
                        ),
                        label: Text(
                          'Change Delivery time',
                          style: TextStyle(fontSize: 20),
                        ))),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    margin: EdgeInsets.only(top: 0, right: 10),
                    child: IconButton(
                      icon: Icon(
                        Icons.menu,
                        size: 30,
                        color: Color(0xFF212529),
                      ),
                      onPressed: () {
                        //_scaffoldKey.currentState.openDrawer();
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
              child: ListView(
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: deviceSize.height / 4 / 4),
                      child: FaIcon(
                        FontAwesomeIcons.calendarAlt,
                        color: Colors.green,
                        size: 100,
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            'Your current Slot',
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w400,
                                fontSize: 19),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Text(
                            'Tomorrow,6AM - 10AM',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 19),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ]),
              Container(
                margin: EdgeInsets.only(
                  top: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    for (int index1 = 0; index1 < dates.length; index1++)
                      GestureDetector(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          color: dates[index1]['selected'] == true
                              ? Colors.white
                              : Colors.grey[200],
                          child: FittedBox(
                            child: Column(
                              children: [
                                Text(
                                  dates[index1]['day'],
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  dates[index1]['date'],
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () => setState(() {
                          dates[index1]['selected'] =
                              !dates[index1]['selected'];
                        }),
                      )
                  ],
                ),
              ),
              for (int index1 = 0; index1 < sortby.length; index1++)
                GestureDetector(
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    // margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.grey.withOpacity(.3))),

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Wrap(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 3),
                                  child: Icon(
                                    Icons.schedule,
                                    color: Colors.grey,
                                    size: 15,
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  sortby[index1]['Answer'],
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18),
                                ),
                              ],
                            )),
                        Container(
                          width: 20,
                          height: 20,
                          margin: EdgeInsets.only(right: 20),
                          child: sortby[index1]['selected'] == true
                              ? Container(
                                  margin: EdgeInsets.all(8),
                                  width: 5,
                                  height: 5,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                  ),
                                )
                              : null,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: sortby[index1]['selected'] == true
                                  ? Colors.green
                                  : Colors.white,
                              border: Border.all(color: Colors.grey)),
                        )
                      ],
                    ),
                  ),
                  onTap: () => setState(() {
                    sortby[index1]['selected'] = !sortby[index1]['selected'];
                    choosedvalue = !choosedvalue;
                  }),
                )
            ],
          )),
          GestureDetector(
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              margin: EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 10),
              child: Container(
                width: double.infinity,
                height: deviceSize.height / 4 / 2 * .7,
                alignment: Alignment.center,
                child: Text(
                  'Schedule Order',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                decoration: BoxDecoration(
                    color: Color(0xFF19e81c),
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                return PaymentMethodScreen();
              }));
            },
          ),
        ],
      )),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
        child: FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed: () {},
          child: FaIcon(
            FontAwesomeIcons.solidMoon,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
