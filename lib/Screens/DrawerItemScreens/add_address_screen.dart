import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AddAddressScreen extends StatefulWidget {
  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  bool isHomeSelected = true;
  bool isWorkSelcted = false;
  bool isOtherSelected = false;
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        body: SafeArea(
            child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [
              Expanded(
                  child: ListView(
                children: [
                  Card(
                    elevation: 0,
                    child: Container(
                      width: double.infinity,
                      height: deviceSize.height / 4 / 3,
                      color: Color(0xFFF0F2F5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.only(left: 15),
                              child: Text(
                                'Add Delivery Address',
                                style: TextStyle(
                                    color: Color(0xFF212529),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 20),
                              )),
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              padding: EdgeInsets.all(5),
                              child: IconButton(
                                icon: Icon(Icons.cancel),
                                onPressed: () {
                                  Navigator.of(context).pop(true);
                                },
                              ))
                        ],
                      ),
                    ),
                  ),
                  Form(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                        child: Text(
                          'Delivery Area',
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: TextFormField(
                            decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 10),
                          hintText: 'Delivery Area',
                          suffixIcon: IconButton(
                            icon: Icon(Icons.location_history),
                            onPressed: () {},
                          ),
                          hintStyle: TextStyle(color: Colors.grey),
                        )),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                        child: Text(
                          'Complete Address',
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: TextFormField(
                            decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 10),
                          hintText: 'Complete Address E.g house no .2 ',
                          hintStyle: TextStyle(color: Colors.grey),
                        )),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                        child: Text(
                          'Delivery Instructions',
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: TextFormField(
                            decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 10),
                          hintText:
                              'Delivery Instructions e.g opposite Gold sluck',
                          hintStyle: TextStyle(color: Colors.grey),
                        )),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15, left: 20),
                        child: Text('Nickname'),
                      ),
                      Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(
                              top: 15,
                              left: deviceSize.width / 4 / 3,
                              right: deviceSize.width / 4 / 3),
                          decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.grey)),
                          child: Row(
                            children: [
                              Expanded(
                                  child: GestureDetector(
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(10),
                                  color: isHomeSelected == true
                                      ? Colors.grey
                                      : Colors.white,
                                  child: Text(
                                    'Home',
                                    style: TextStyle(
                                        color: isHomeSelected == true
                                            ? Colors.white
                                            : Colors.grey),
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    isHomeSelected = !isHomeSelected;
                                    isOtherSelected = false;
                                    isWorkSelcted = false;
                                  });
                                },
                              )),
                              Expanded(
                                  child: GestureDetector(
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(10),
                                  color: isWorkSelcted == true
                                      ? Colors.grey
                                      : Colors.white,
                                  child: Text(
                                    'Work',
                                    style: TextStyle(
                                        color: isWorkSelcted == true
                                            ? Colors.white
                                            : Colors.grey),
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    isWorkSelcted = !isWorkSelcted;
                                    isHomeSelected = false;
                                    isOtherSelected = false;
                                  });
                                },
                              )),
                              Expanded(
                                  child: GestureDetector(
                                child: Container(
                                  alignment: Alignment.center,
                                  color: isOtherSelected == true
                                      ? Colors.grey
                                      : Colors.white,
                                  padding: EdgeInsets.all(10),
                                  child: Text(
                                    'Other',
                                    style: TextStyle(
                                        color: isOtherSelected == true
                                            ? Colors.white
                                            : Colors.grey),
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    isOtherSelected = !isOtherSelected;
                                    isWorkSelcted = false;
                                    isHomeSelected = false;
                                  });
                                },
                              ))
                            ],
                          ))
                    ],
                  ))
                ],
              )),
              Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        height: deviceSize.height / 4 / 2 * .7,
                        alignment: Alignment.center,
                        child: Text('Close'),
                        color: Color(0xFFF0F2F5),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: deviceSize.height / 4 / 2 * .7,
                      alignment: Alignment.center,
                      child: Text(
                        'Save Changes',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.green,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        )),
        floatingActionButton: Container(
          margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
          child: FloatingActionButton(
            backgroundColor: Colors.black,
            onPressed: () {},
            child: FaIcon(
              FontAwesomeIcons.solidMoon,
              color: Colors.white,
            ),
          ),
        ));
  }
}
