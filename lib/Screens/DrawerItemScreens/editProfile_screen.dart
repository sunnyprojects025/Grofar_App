import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/DrawerItemScreens/changePassword_screen.dart';
import 'package:grofar/Screens/DrawerItemScreens/deactivateAccount_screen.dart';

class EditProfileScreen extends StatelessWidget {
  final List editcards = [
    {
      'title': 'Change Password',
      'widget': ChangePasswordScreen(),
    },
    {
      'title': 'Deactivate Account',
      'widget': DeactivateAccountScreen(),
    },
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFF0F2F5),
      body: SafeArea(
          child: ListView(
        children: [
          Card(
            color: Color(0xFFF0F2F5),
            elevation: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    //  color: Colors.white,
                    padding: EdgeInsets.all(5),
                    child: FlatButton.icon(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.green,
                          size: 20,
                        ),
                        label: Text(
                          'Edit Profile',
                          style: TextStyle(fontSize: 20),
                        ))),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    margin: EdgeInsets.only(top: 0, right: 8),
                    child: IconButton(
                      icon: Icon(
                        Icons.menu,
                        size: 30,
                        color: Color(0xFF212529),
                      ),
                      onPressed: () {
                        // _scaffoldKey.currentState.openDrawer();
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Column(children: [
              Container(
                margin: EdgeInsets.only(top: deviceSize.height / 4 / 4 / 2),
                child: CircleAvatar(
                  radius: 50,
                  backgroundImage: AssetImage(
                    'assets/img/user.jpg',
                  ),
                ),
              ),
              Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      'Askbootstrap',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 19),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text('iamsohan@gmail.com')
                  ],
                ),
              ),
            ])
          ]),
          Form(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15, top: 10),
                child: Text(
                  'Full Name',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: TextFormField(
                    decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(left: 15),
                  hintText: 'Askbootstrap',
                  hintStyle: TextStyle(color: Colors.grey),
                )),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, top: 10),
                child: Text(
                  'Mobile Number',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: TextFormField(
                    decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(left: 15),
                  hintText: '12345667',
                  hintStyle: TextStyle(color: Colors.grey),
                )),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, top: 10),
                child: Text(
                  'Email',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: TextFormField(
                    decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(left: 15),
                  hintText: 'iamsohan@gmail.com',
                  hintStyle: TextStyle(color: Colors.grey),
                )),
              ),
              GestureDetector(
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  margin:
                      EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 10),
                  child: Container(
                    width: double.infinity,
                    height: deviceSize.height / 4 / 2 * .7,
                    alignment: Alignment.center,
                    child: Text(
                      'Save Changes',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    decoration: BoxDecoration(
                        color: Color(0xFF19e81c),
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ),
                onTap: () {},
              ),
              for (int index1 = 0; index1 < editcards.length; index1++)
                GestureDetector(
                  child: Card(
                    elevation: 1,
                    margin: EdgeInsets.only(
                      left: 5,
                      top: 3,
                      bottom: 3,
                      right: 5,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, bottom: 10),
                          padding: EdgeInsets.only(
                              left: 10, right: 10, top: 15, bottom: 15),
                          child: Text(editcards[index1]['title'],
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  color: Colors.grey,
                                  fontSize: 16)),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.green,
                            size: 15,
                          ),
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                      return editcards[index1]['widget'] == null
                          ? null
                          : editcards[index1]['widget'];
                    }));
                  },
                )
            ],
          ))
        ],
      )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
