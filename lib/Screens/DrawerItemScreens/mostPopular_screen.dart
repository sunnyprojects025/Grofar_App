import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MostPopularScreen extends StatelessWidget {
  final List _pickoffers = [
    {'image': 'assets/img/green/g8.jpg', 'title': 'Brocolli'},
    {'image': 'assets/img/green/g8.jpg', 'title': 'Brocolli'},
    {'image': 'assets/img/green/g8.jpg', 'title': 'Brocolli'},
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
          child: Container(
        width: double.infinity,
        height: double.infinity,
        child: ListView(
          children: [
            Card(
              elevation: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.all(5),
                      child: FlatButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                            size: 20,
                          ),
                          label: Text(
                            'Favorite',
                            style: TextStyle(fontSize: 20),
                          ))),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      margin: EdgeInsets.only(top: 6, right: 10),
                      child: Icon(
                        Icons.menu,
                        size: 30,
                        color: Color(0xFF212529),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Text(
                      'Green Vegan',
                      style: TextStyle(
                          color: Color(0xFF212529),
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                          letterSpacing: .8),
                    ),
                  ),
                  GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Text(
                        'See More',
                        style: TextStyle(
                            color: Colors.green,
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            letterSpacing: .8),
                      ),
                    ),
                    onTap: () {},
                  )
                ],
              ),
            ),
            Container(
                width: deviceSize.width,
                height: deviceSize.height / 2 * .9,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: _pickoffers.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      child: Container(
                        width: deviceSize.width * .8,
                        height: deviceSize.height / 3,
                        margin: EdgeInsets.all(5),
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Container(
                              padding: EdgeInsets.all(5),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: deviceSize.width * .7,
                                      height: deviceSize.height / 3 * .6,
                                      child: Stack(
                                        children: [
                                          Positioned(
                                              top: 10,
                                              left: deviceSize.width / 4 / 2,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color:
                                                      Colors.orangeAccent[100],
                                                ),
                                                child: Text(
                                                  "25%",
                                                  style: TextStyle(
                                                      color:
                                                          Colors.orange[700]),
                                                ),
                                              )),
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  _pickoffers[index]['image']),
                                              fit: BoxFit.fill)),
                                    ),
                                    Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.all(5),
                                            margin: EdgeInsets.only(
                                                left: 10, top: 10),
                                            child: Text(
                                              _pickoffers[index]['title'],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.green,
                                                  fontSize: 18),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(10),
                                            child: Text(
                                              '\$ 1/kg',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 18),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(5),
                                            child: Text(
                                              'fresh Brocolli from thailand 200 gm',
                                              style: TextStyle(fontSize: 16),
                                            ),
                                          ),
                                        ]),
                                  ])),
                        ),
                      ),
                      onTap: () {},
                    );
                  },
                )),
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
