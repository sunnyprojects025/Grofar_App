import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TermsConditionsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Card(
              elevation: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.all(5),
                      child: FlatButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                            size: 20,
                          ),
                          label: Text(
                            'Terms & Conditions',
                            style: TextStyle(fontSize: 20),
                          ))),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      margin: EdgeInsets.only(top: 0, right: 8),
                      child: IconButton(
                        icon: Icon(
                          Icons.menu,
                          size: 30,
                          color: Color(0xFF212529),
                        ),
                        onPressed: () {
                          // _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: deviceSize.height / 4 / 4 / 2,
                  left: deviceSize.width / 4 / 4),
              child: Text(
                'Terms & conditions',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Container(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
              child: Text(
                  'Grocery may give refunds for some item purchases, depending on the refund policies. You can also contact us directly If a purchase was accidentally made by a friend or family member using your account, request a refund on the grocery website.If you find a grocery purchase on your card or other payment method that you didnt make and that wasnt made by anyone you know, report unauthorized charges within 120 days of the transaction.If you\'ve had a refund request accepted, check how long your refund will take.',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Colors.grey,
                      fontSize: 18)),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
