import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/bottomNavigationScreens/cartScreen.dart';

class PaymentMethodScreen extends StatefulWidget {
  @override
  _PaymentMethodScreenState createState() => _PaymentMethodScreenState();
}

class _PaymentMethodScreenState extends State<PaymentMethodScreen> {
  bool checked = false;
  bool cashchecked = false;
  bool creditcardopen = false;
  bool netbankingopen = false;
  bool cashdeliveryopen = false;
  bool isHomeSelected = true;
  bool isWorkSelcted = false;
  bool isOtherSelected = false;
  String textvalue = 'Bank';

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: Color(0xFFF0F2F5),
        body: SafeArea(
          child: Column(
            children: [
              Card(
                elevation: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: FlatButton.icon(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.green,
                              size: 20,
                            ),
                            label: Text(
                              'Payment Method',
                              style: TextStyle(fontSize: 20),
                            ))),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Container(
                        margin: EdgeInsets.only(top: 0, right: 10),
                        child: IconButton(
                          icon: Icon(
                            Icons.menu,
                            size: 30,
                            color: Color(0xFF212529),
                          ),
                          onPressed: () {
                            //_scaffoldKey.currentState.openDrawer();
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                  child: ListView(
                children: [
                  Card(
                    elevation: 1,
                    margin: EdgeInsets.only(
                      left: deviceSize.width / 4 / 4,
                      top: 3,
                      bottom: 3,
                      right: deviceSize.width / 4 / 4,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 15, bottom: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: Icon(Icons.card_travel_sharp,
                                      color: Colors.green),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text("Credit/Debit Card",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey,
                                        fontSize: 16)),
                              ],
                            )),
                        GestureDetector(
                          child: Container(
                            margin: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.arrow_downward_sharp,
                              color: Colors.green,
                              size: 15,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              creditcardopen = !creditcardopen;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  creditcardopen == true
                      ? Card(
                          margin: EdgeInsets.only(
                            left: deviceSize.width / 4 / 4,
                            right: deviceSize.width / 4 / 4,
                          ),
                          child: Form(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 10),
                                child: Text(
                                  'Card number',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(10),
                                child: TextFormField(
                                    decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 15),
                                  hintText: 'Enter card Number',
                                  hintStyle: TextStyle(color: Colors.grey),
                                )),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 10),
                                child: Text(
                                  'Valid through',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(10),
                                child: TextFormField(
                                    decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 15),
                                  hintText: 'Enter Valid through(mm/yy',
                                  hintStyle: TextStyle(color: Colors.grey),
                                )),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 10),
                                child: Text(
                                  'CVV',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(10),
                                child: TextFormField(
                                    decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 15),
                                  hintText: 'Enter Cvv Number',
                                  hintStyle: TextStyle(color: Colors.grey),
                                )),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 10),
                                child: Text(
                                  'Name On card',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(10),
                                child: TextFormField(
                                    decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 15),
                                  hintText: 'Enter Name On card Number',
                                  hintStyle: TextStyle(color: Colors.grey),
                                )),
                              ),
                              Container(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  children: [
                                    Transform.scale(
                                      scale: 1.2,
                                      child: Checkbox(
                                        focusColor: Colors.lightBlue,
                                        activeColor: Color(0xffEB4DAC),
                                        value: checked,
                                        onChanged: (newValue) {
                                          setState(() {
                                            checked = (newValue);
                                            print(checked);
                                          });
                                        },
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      child: FittedBox(
                                        child: Text(
                                            'Securely save this card for faster checkout next time'),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          )),
                        )
                      : Container(),
                  Card(
                    elevation: 1,
                    margin: EdgeInsets.only(
                      left: deviceSize.width / 4 / 4,
                      top: 3,
                      bottom: 3,
                      right: deviceSize.width / 4 / 4,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 15, bottom: 15),
                            child: Row(
                              children: [
                                Icon(Icons.card_travel_sharp,
                                    color: Colors.green),
                                SizedBox(
                                  width: 10,
                                ),
                                Text("Net Banking",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey,
                                        fontSize: 16)),
                              ],
                            )),
                        GestureDetector(
                          child: Container(
                            margin: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.arrow_downward_sharp,
                              color: Colors.green,
                              size: 15,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              netbankingopen = !netbankingopen;
                            });
                          },
                        )
                      ],
                    ),
                  ),
                  netbankingopen == true
                      ? Card(
                          margin: EdgeInsets.only(
                            left: deviceSize.width / 4 / 4,
                            right: deviceSize.width / 4 / 4,
                          ),
                          child: Form(
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                Container(
                                    width: double.infinity,
                                    margin: EdgeInsets.only(
                                        top: 15,
                                        left: deviceSize.width / 4 / 3,
                                        right: deviceSize.width / 4 / 3),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            width: 1, color: Colors.grey)),
                                    child: Row(
                                      children: [
                                        Expanded(
                                            child: GestureDetector(
                                          child: Container(
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.all(10),
                                            color: isHomeSelected == true
                                                ? Colors.grey
                                                : Colors.white,
                                            child: Text(
                                              'HDFC',
                                              style: TextStyle(
                                                  color: isHomeSelected == true
                                                      ? Colors.white
                                                      : Colors.grey),
                                            ),
                                          ),
                                          onTap: () {
                                            setState(() {
                                              isHomeSelected = !isHomeSelected;
                                              isOtherSelected = false;
                                              isWorkSelcted = false;
                                            });
                                          },
                                        )),
                                        Expanded(
                                            child: GestureDetector(
                                          child: Container(
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.all(10),
                                            color: isWorkSelcted == true
                                                ? Colors.grey
                                                : Colors.white,
                                            child: Text(
                                              'ICIC',
                                              style: TextStyle(
                                                  color: isWorkSelcted == true
                                                      ? Colors.white
                                                      : Colors.grey),
                                            ),
                                          ),
                                          onTap: () {
                                            setState(() {
                                              isWorkSelcted = !isWorkSelcted;
                                              isHomeSelected = false;
                                              isOtherSelected = false;
                                            });
                                          },
                                        )),
                                        Expanded(
                                            child: GestureDetector(
                                          child: Container(
                                            alignment: Alignment.center,
                                            color: isOtherSelected == true
                                                ? Colors.grey
                                                : Colors.white,
                                            padding: EdgeInsets.all(10),
                                            child: Text(
                                              'Axis',
                                              style: TextStyle(
                                                  color: isOtherSelected == true
                                                      ? Colors.white
                                                      : Colors.grey),
                                            ),
                                          ),
                                          onTap: () {
                                            setState(() {
                                              isOtherSelected =
                                                  !isOtherSelected;
                                              isWorkSelcted = false;
                                              isHomeSelected = false;
                                            });
                                          },
                                        ))
                                      ],
                                    )),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 10, top: 20, bottom: 10),
                                  child: Text(
                                    'Select Bank',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                ),
                                Container(
                                    margin: EdgeInsets.all(10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Expanded(
                                            child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding:
                                                  EdgeInsets.only(left: 20),
                                              child: Text(
                                                textvalue,
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            Divider()
                                          ],
                                        )),
                                        DropdownButton<String>(
                                          items: <String>['A', 'B', 'C', 'D']
                                              .map((String value) {
                                            return new DropdownMenuItem<String>(
                                              value: value,
                                              child: new Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (value) {
                                            setState(() {
                                              value = textvalue;
                                            });
                                          },
                                        )
                                      ],
                                    )),
                              ])))
                      : Container(),
                  Card(
                    elevation: 1,
                    margin: EdgeInsets.only(
                      left: deviceSize.width / 4 / 4,
                      top: 3,
                      bottom: 3,
                      right: deviceSize.width / 4 / 4,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 15, bottom: 15),
                            child: Row(
                              children: [
                                Icon(Icons.money, color: Colors.green),
                                SizedBox(
                                  width: 10,
                                ),
                                Text("Cash on Delivery",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey,
                                        fontSize: 16)),
                              ],
                            )),
                        GestureDetector(
                          child: Container(
                            margin: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.arrow_downward_sharp,
                              color: Colors.green,
                              size: 15,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              cashdeliveryopen = !cashdeliveryopen;
                            });
                          },
                        )
                      ],
                    ),
                  ),
                  cashdeliveryopen == true
                      ? Card(
                          margin: EdgeInsets.only(
                            left: deviceSize.width / 4 / 4,
                            right: deviceSize.width / 4 / 4,
                          ),
                          child: Container(
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                            child: Form(
                                child: ListTile(
                              leading: Transform.scale(
                                scale: 1.2,
                                child: Checkbox(
                                  focusColor: Colors.lightBlue,
                                  activeColor: Color(0xffEB4DAC),
                                  value: cashchecked,
                                  onChanged: (newValue) {
                                    setState(() {
                                      cashchecked = (newValue);
                                      print(checked);
                                    });
                                  },
                                ),
                              ),
                              title: Container(
                                  margin: EdgeInsets.only(
                                      top: deviceSize.height / 4 / 4 / 2),
                                  child: Text('Cash')),
                              subtitle: Text(
                                  'Please keep Exact changed Handy Please keep Exact changed HandyPlease keep Exact changed Handy'),
                            )),
                          ),
                        )
                      : Container()
                ],
              )),
              GestureDetector(
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    margin:
                        EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 10),
                    child: Container(
                      width: double.infinity,
                      height: deviceSize.height / 4 / 2 * .7,
                      alignment: Alignment.center,
                      child: Text(
                        'Continue',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      decoration: BoxDecoration(
                          color: Color(0xFF19e81c),
                          borderRadius: BorderRadius.circular(5)),
                    ),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                      return CartScreen();
                    }));
                  }),
            ],
          ),
        ),
        floatingActionButton: Container(
          margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
          child: FloatingActionButton(
            backgroundColor: Colors.black,
            onPressed: () {},
            child: FaIcon(
              FontAwesomeIcons.solidMoon,
              color: Colors.white,
            ),
          ),
        ));
  }
}
