import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HelpTicketScreen extends StatelessWidget {
  final List helpSupport = [
    'Have to check my order status?',
    'change items in my order?',
    'cancel my order?',
    'changes my delivery address?',
    'help with a pick order?',
    'Refunding Payment'
  ];

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFF0F2F5),
      body: SafeArea(
        child: Column(
          children: [
            Card(
              color: Colors.white,
              elevation: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      //  color: Colors.white,
                      padding: EdgeInsets.all(5),
                      child: FlatButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                            size: 20,
                          ),
                          label: Text(
                            'Help & Ticket',
                            style: TextStyle(fontSize: 20),
                          ))),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      margin: EdgeInsets.only(top: 0, right: 8),
                      child: IconButton(
                        icon: Icon(
                          Icons.menu,
                          size: 30,
                          color: Color(0xFF212529),
                        ),
                        onPressed: () {
                          // _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
                child: Form(
              child: ListView(
                children: [
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, top: 10),
                    child: Text(
                      'Name',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.grey),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: TextFormField(
                        decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10),
                      hintText: 'Enter Name',
                      hintStyle: TextStyle(color: Colors.grey),
                    )),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, top: 10),
                    child: Text(
                      'Email',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.grey),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: TextFormField(
                        decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10),
                      hintText: 'Enter Enter Email',
                      hintStyle: TextStyle(color: Colors.grey),
                    )),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, top: 10),
                    child: Text(
                      'How can we improve',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.grey),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: TextFormField(
                        decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10),
                      hintText: 'can we improve?',
                      hintStyle: TextStyle(color: Colors.grey),
                    )),
                  ),
                ],
              ),
            )),
            GestureDetector(
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                margin: EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 10),
                child: Container(
                  width: double.infinity,
                  height: deviceSize.height / 4 / 2 * .7,
                  alignment: Alignment.center,
                  child: Text(
                    'Ask us Freely',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  decoration: BoxDecoration(
                      color: Color(0xFF19e81c),
                      borderRadius: BorderRadius.circular(5)),
                ),
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
        child: FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed: () {},
          child: FaIcon(
            FontAwesomeIcons.solidMoon,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
