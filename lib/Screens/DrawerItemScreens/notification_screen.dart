import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NotifiactionScreen extends StatelessWidget {
  final List notification = [
    {
      'title': 'Yay! Your Order Complete',
      'subtitle':
          'Hooray Your order is copleted Now you can check on your own..',
      'time': '05:14 Am'
    },
    {
      'title': 'Yay! Your Order Complete',
      'subtitle':
          'Hooray Your order is copleted Now you can check on your own..',
      'time': '05:14 Am'
    },
    {
      'title': 'Yay! Your Order Complete',
      'subtitle':
          'Hooray Your order is copleted Now you can check on your own..',
      'time': '05:14 Am'
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        width: double.infinity,
        height: double.infinity,
        child: ListView(children: [
          Card(
            elevation: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    padding: EdgeInsets.all(5),
                    child: FlatButton.icon(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.green,
                          size: 20,
                        ),
                        label: Text(
                          'Notification',
                          style: TextStyle(fontSize: 20),
                        ))),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    margin: EdgeInsets.only(top: 6, right: 10),
                    child: Icon(
                      Icons.menu,
                      size: 30,
                      color: Color(0xFF212529),
                    ),
                  ),
                )
              ],
            ),
          ),
          for (int index1 = 0; index1 < notification.length; index1++)
            Card(
              child: Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      notification[index1]['title'],
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(notification[index1]['subtitle']),
                    SizedBox(
                      height: 15,
                    ),
                    FlatButton.icon(
                        onPressed: () {},
                        icon: Icon(Icons.calendar_today),
                        label: Text(notification[index1]['time']))
                  ],
                ),
              ),
            ),
        ]),
      )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
