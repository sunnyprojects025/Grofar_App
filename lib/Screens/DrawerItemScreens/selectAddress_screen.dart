import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:grofar/Screens/DrawerItemScreens/add_address_screen.dart';

class SelectAddressScreen extends StatelessWidget {
  final List address = [
    {
      'title': 'Home',
      'subtitle': 'Thane ,building shakuntala road  Mumbai Maharashtra',
      'status': 'Default'
    },
    {
      'title': 'Work',
      'subtitle': 'Thane ,building shakuntala road  Mumbai Maharashtra',
      'status': 'other'
    }
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
        body: SafeArea(
            child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Container(
                  padding: EdgeInsets.all(10),
                  child: Wrap(
                    direction: Axis.horizontal,
                    children: [
                      Container(
                          padding: EdgeInsets.all(5),
                          child: FlatButton.icon(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.green,
                                size: 20,
                              ),
                              label: Text(
                                'Select Address',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ))),
                    ],
                  ),
                ),
                Wrap(
                  children: [
                    GestureDetector(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.green),
                            borderRadius: BorderRadius.circular(5)),
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Add',
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (ctx) {
                          return AddAddressScreen();
                        }));
                      },
                    ),
                    Container(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          child: IconButton(
                            icon: Icon(
                              Icons.menu,
                              size: 30,
                              color: Color(0xFF212529),
                            ),
                            onPressed: () {},
                          ),
                        )),
                  ],
                ),
              ]),
              Expanded(
                  child: ListView(
                children: [
                  for (int index1 = 0; index1 < address.length; index1++)
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      margin: EdgeInsets.all(10),
                      child: Container(
                          padding: EdgeInsets.only(
                              top: 15, bottom: 15, left: 10, right: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: Colors.green,
                              )),
                          child: Column(children: [
                            ListTile(
                                title: Text(
                                  address[index1]['title'],
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                                subtitle: Container(
                                  width: deviceSize.width / 2,
                                  padding: EdgeInsets.all(5),
                                  child: Text(address[index1]['subtitle']),
                                ),
                                trailing: address[index1]['status'] == 'Default'
                                    ? Container(
                                        decoration: BoxDecoration(
                                            color: Colors.green.withOpacity(.3),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        padding: EdgeInsets.all(5),
                                        margin: EdgeInsets.only(bottom: 20),
                                        child: Text(
                                          address[index1]['status'],
                                          style: TextStyle(color: Colors.green),
                                        ),
                                      )
                                    : Container(
                                        decoration: BoxDecoration(
                                            color: Colors.green.withOpacity(.3),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        padding: EdgeInsets.all(5),
                                        margin: EdgeInsets.only(bottom: 20),
                                        child: Text(
                                          address[index1]['status'],
                                          style: TextStyle(color: Colors.green),
                                        ),
                                      )),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                GestureDetector(
                                    child: Container(
                                  margin: EdgeInsets.only(right: 15),
                                  child: Text(
                                    'Edit',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ))
                              ],
                            )
                          ])),
                    )
                ],
              )),
              GestureDetector(
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  margin:
                      EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 10),
                  child: Container(
                    width: double.infinity,
                    height: deviceSize.height / 4 / 2 * .7,
                    alignment: Alignment.center,
                    child: Text(
                      'Continue',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    decoration: BoxDecoration(
                        color: Color(0xFF19e81c),
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ),
                onTap: () {},
              ),
            ],
          ),
        )),
        floatingActionButton: Container(
          margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
          child: FloatingActionButton(
            backgroundColor: Colors.black,
            onPressed: () {},
            child: FaIcon(
              FontAwesomeIcons.solidMoon,
              color: Colors.white,
            ),
          ),
        ));
  }
}
