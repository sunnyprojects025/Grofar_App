import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FeedBackScreen extends StatefulWidget {
  @override
  _FeedBackScreenState createState() => _FeedBackScreenState();
}

class _FeedBackScreenState extends State<FeedBackScreen> {
  bool isHomeSelected = true;
  bool isWorkSelcted = false;
  bool isOtherSelected = false;
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
          child: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            Expanded(
                child: ListView(
              children: [
                Card(
                  elevation: 2,
                  child: Container(
                    height: deviceSize.height / 4 / 2 * .7,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.green,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Container(
                            margin: EdgeInsets.only(top: 6, right: 10),
                            child: Icon(
                              Icons.menu,
                              size: 30,
                              color: Color(0xFF212529),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(
                      top: deviceSize.height / 4 / 2, left: 10, right: 10),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Rate Your Order Experience',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                              'Give me what you feel after you finish your order'),
                        ],
                      ),
                      SizedBox(
                        height: deviceSize.height / 4 / 3,
                      ),
                      Container(
                          width: deviceSize.width / 2,
                          height: deviceSize.height / 4 / 4,
                          margin: EdgeInsets.only(
                              top: 15,
                              left: deviceSize.width / 4 / 3,
                              right: deviceSize.width / 4 / 3),
                          decoration: BoxDecoration(
                              //borderRadius: BorderRadius.circular(10),
                              border: Border.all(width: 1, color: Colors.grey)),
                          child: Row(
                            children: [
                              Expanded(
                                  child: GestureDetector(
                                child: Container(
                                    alignment: Alignment.center,
                                    color: isHomeSelected == true
                                        ? Colors.green
                                        : Colors.white,
                                    padding: EdgeInsets.all(5),
                                    child: FaIcon(FontAwesomeIcons.smile,
                                        color: isHomeSelected == true
                                            ? Colors.white
                                            : Colors.grey)),
                                onTap: () {
                                  setState(() {
                                    isHomeSelected = !isHomeSelected;
                                    isOtherSelected = false;
                                    isWorkSelcted = false;
                                  });
                                },
                              )),
                              Expanded(
                                  child: GestureDetector(
                                child: Container(
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.all(5),
                                    color: isWorkSelcted == true
                                        ? Colors.green
                                        : Colors.white,
                                    child: FaIcon(FontAwesomeIcons.sadTear,
                                        color: isWorkSelcted == true
                                            ? Colors.white
                                            : Colors.grey)),
                                onTap: () {
                                  setState(() {
                                    isWorkSelcted = !isWorkSelcted;
                                    isHomeSelected = false;
                                    isOtherSelected = false;
                                  });
                                },
                              )),
                              Expanded(
                                  child: GestureDetector(
                                child: Container(
                                    alignment: Alignment.center,
                                    color: isOtherSelected == true
                                        ? Colors.green
                                        : Colors.white,
                                    padding: EdgeInsets.all(5),
                                    child: FaIcon(
                                      FontAwesomeIcons.sadCry,
                                      color: isOtherSelected == true
                                          ? Colors.white
                                          : Colors.grey,
                                    )),
                                onTap: () {
                                  setState(() {
                                    isOtherSelected = !isOtherSelected;
                                    isWorkSelcted = false;
                                    isHomeSelected = false;
                                  });
                                },
                              ))
                            ],
                          ))
                    ],
                  ),
                )
              ],
            )),
            GestureDetector(
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                margin: EdgeInsets.only(left: 5, right: 5, bottom: 2, top: 10),
                child: Container(
                  width: double.infinity,
                  height: deviceSize.height / 4 / 2 * .7,
                  alignment: Alignment.center,
                  child: Text(
                    'Submit',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  decoration: BoxDecoration(
                      color: Color(0xFF19e81c),
                      borderRadius: BorderRadius.circular(5)),
                ),
              ),
              onTap: () {},
            ),
          ],
        ),
      )),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
        child: FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed: () {},
          child: FaIcon(
            FontAwesomeIcons.solidMoon,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
