import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../Screens/bottomNavigation_Screen.dart';
import 'bottomNavigationScreens/shopScreen.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  List details = [
    {"title": 'Grocery', "subtitle": 'Your daily needs'},
    {
      "title": 'Best prices & Offers',
      "subtitle": 'cheaper prices than your local supermarket'
    },
    {
      "title": 'wide Assortment',
      "subtitle": 'choose from 5000+ products across food Personal care'
    },
    {
      "title": 'Easy Returns',
      "subtitle":
          'not satisfied with products? Return it at the doorstep not satisfied with products?'
    },
  ];
  List<bool> isquizAnswerButtonClicked = [false, false, false];
  PageController _controller = PageController(
    initialPage: 0,
  );

  void ontapped(int pageindex) {
    _controller.animateToPage(pageindex,
        duration: Duration(milliseconds: 400), curve: Curves.easeInCubic);
    _controller.jumpToPage(pageindex + 1);
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: PageView.builder(
              controller: _controller,
              itemBuilder: (context, index) {
                return Container(
                  width: deviceSize.width,
                  height: deviceSize.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      index > 0
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  width: deviceSize.width / 4,
                                  height: deviceSize.height / 4 / 4,
                                  margin: EdgeInsets.only(
                                      right: 15,
                                      top: deviceSize.height / 4 / 4 / 2),
                                  child: FittedBox(
                                    child: FlatButton.icon(
                                        onPressed: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(builder: (ctx) {
                                            return BottomNavigationScreen();
                                          }));
                                        },
                                        icon: Icon(
                                          Icons.arrow_forward,
                                          size: 17,
                                          color: Colors.green,
                                        ),
                                        label: Text(
                                          'Skip',
                                          style: TextStyle(
                                              color: Colors.green,
                                              fontWeight: FontWeight.bold),
                                        )),
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 2, color: Colors.green)),
                                )
                              ],
                            )
                          : Container(),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: deviceSize.width / 4,
                              height: deviceSize.height / 4 / 2,
                              child: Image.asset(
                                'assets/logos/logo.png',
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20)),
                            ),
                            FittedBox(
                              child: Text(
                                details[index]['title'],
                                style: TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 30),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: 10,
                                  bottom: 10,
                                  left: deviceSize.width / 4 / 2,
                                  right: deviceSize.width / 4 / 2),
                              child: Text(
                                details[index]['subtitle'],
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.w400),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ...details.map((e) => Container(
                                    width: 8,
                                    height: 8,
                                    margin: EdgeInsets.only(
                                        bottom: deviceSize.height / 4 / 4,
                                        left: 15,
                                        right: 15),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.grey),
                                  ))
                            ],
                          ),
                          GestureDetector(
                              child: Card(
                                elevation: 0,
                                margin: EdgeInsets.only(
                                    left: 5, right: 5, bottom: 1),
                                child: Container(
                                  width: double.infinity,
                                  height: deviceSize.height / 4 / 2 * .7,
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Get Started',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  decoration: BoxDecoration(
                                    color: Color(0xFF19e81c),
                                  ),
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  index != details.length - 1
                                      ? ontapped(index)
                                      : Navigator.push(context,
                                          MaterialPageRoute(builder: (ctx) {
                                          return BottomNavigationScreen();
                                        }));
                                });
                              })
                        ],
                      )
                    ],
                  ),
                );
              },
              itemCount: details.length)),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 3),
        child: FloatingActionButton(
          backgroundColor: Colors.black,
          onPressed: () {},
          child: FaIcon(
            FontAwesomeIcons.solidMoon,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
