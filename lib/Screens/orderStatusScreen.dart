import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class OrderStatusScreen extends StatelessWidget {
  final String id, cost;

  OrderStatusScreen({this.id, this.cost});
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: ListView(
        children: [
          Card(
            elevation: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    padding: EdgeInsets.all(5),
                    child: FlatButton.icon(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.green,
                          size: 20,
                        ),
                        label: Text(
                          id == null ? 'ID #424334' : 'ID ' + id,
                          style: TextStyle(fontSize: 20),
                        ))),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    margin: EdgeInsets.only(top: 6, right: 10),
                    child: Icon(
                      Icons.menu,
                      size: 30,
                      color: Color(0xFF212529),
                    ),
                  ),
                )
              ],
            ),
          ),
          Card(
            child: Container(
              padding: EdgeInsets.all(15),
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                FlatButton.icon(
                    onPressed: null,
                    icon: Icon(Icons.calendar_today),
                    label: Text(
                      '06/05/21 11.00AM',
                      style: TextStyle(color: Colors.black),
                    )),
              ]),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Order Status',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                FlatButton.icon(
                    onPressed: null,
                    icon: Icon(
                      Icons.check,
                      color: Colors.green,
                    ),
                    label: Text('Preparing Order')),
                FlatButton.icon(
                    onPressed: null,
                    icon: Icon(
                      Icons.close,
                      color: Colors.red,
                    ),
                    label: Text('Ready to collect')),
                FlatButton.icon(
                    onPressed: null,
                    icon: Icon(
                      Icons.close,
                      color: Colors.red,
                    ),
                    label: Text('on the way')),
                FlatButton.icon(
                    onPressed: null,
                    icon: Icon(
                      Icons.close,
                      color: Colors.red,
                    ),
                    label: Text('Delivered Order'))
              ],
            ),
          ),
          Card(
            child: Container(
              padding: EdgeInsets.all(15),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Destination',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                        padding: EdgeInsets.all(10),
                        child: Text('554 west 142 nd Street new york')),
                  ]),
            ),
          ),
          Container(
            padding: EdgeInsets.all(15),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                'Courier',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              Container(
                  width: deviceSize.width / 2,
                  child: Row(
                    children: [
                      Container(
                        width: 50,
                        height: 50,
                        child: Image.asset(
                          'assets/logos/logo.png',
                          fit: BoxFit.fill,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: FittedBox(
                          child: Text(
                            'Grocery',
                            style: TextStyle(
                                color: Colors.green,
                                fontWeight: FontWeight.w500,
                                fontSize: 20),
                          ),
                        ),
                      )
                    ],
                  )),
            ]),
          ),
          Card(
            margin: EdgeInsets.only(bottom: deviceSize.height / 4 / 2),
            child: Container(
                padding: EdgeInsets.only(
                    left: 15,
                    right: 15,
                    top: deviceSize.height / 4 / 4 / 2,
                    bottom: deviceSize.height / 4 / 4 / 2),
                child: ListTile(
                  title: Text(
                    'Total Cost',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                  subtitle: Container(
                      width: deviceSize.width / 2,
                      padding: EdgeInsets.all(5),
                      child: Text(
                          'You can check Your Order detail Here Thank you for Order')),
                  trailing: Container(
                    child: Text(
                      cost == null ? '\$6.64' : cost,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                  ),
                )),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {},
        child: FaIcon(
          FontAwesomeIcons.solidMoon,
          color: Colors.white,
        ),
      ),
    );
  }
}
