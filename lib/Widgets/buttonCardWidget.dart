import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ButtonCardWidget extends StatelessWidget {
  final String textString;
  final double cardElevation;
  final IconData buttonIcon;
  final Color buttonColor, iconColor, textColor;
  final Function taphandler;

  ButtonCardWidget(
      {this.textString,
      this.buttonIcon,
      this.buttonColor,
      this.iconColor,
      this.textColor,
      this.taphandler,
      this.cardElevation});

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return GestureDetector(
      child: Card(
        elevation: cardElevation,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.only(left: 5, right: 5, top: 10, bottom: 10),
        child: Container(
          width: double.infinity,
          height: deviceSize.height / 4 / 2 * .7,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FaIcon(
                buttonIcon,
                color: iconColor,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                textString,
                style: TextStyle(color: textColor, fontWeight: FontWeight.bold),
              )
            ],
          ),
          decoration: BoxDecoration(
              color: buttonColor, borderRadius: BorderRadius.circular(10)),
        ),
      ),
      onTap: taphandler,
    );
  }
}
