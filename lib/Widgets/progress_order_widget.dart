import 'package:flutter/material.dart';
import 'package:grofar/Screens/orderStatusScreen.dart';

class ProgressOrderWidget extends StatelessWidget {
  final List progressOrder = [
    {
      'status': 'Progress',
      'time': '06/05/20',
      'transId': '#32574',
      'deliveredTo': 'Home',
      'total': '\$15.87'
    },
    {
      'status': 'Progress',
      'time': '06/05/20',
      'transId': '#32574',
      'deliveredTo': 'Home',
      'total': '\$15.87'
    },
    {
      'status': 'Progress',
      'time': '06/05/20',
      'transId': '#32574',
      'deliveredTo': 'Home',
      'total': '\$15.87'
    }
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return ListView(
      children: [
        for (int index1 = 0; index1 < progressOrder.length; index1++)
          GestureDetector(
            child: Card(
              elevation: 3,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              margin: EdgeInsets.only(
                  top: 10,
                  bottom: 10,
                  left: deviceSize.width / 4 / 3,
                  right: deviceSize.width / 4 / 3),
              child: Container(
                padding:
                    EdgeInsets.only(left: 10, right: 10, bottom: 15, top: 15),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                width: double.infinity,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.all(7),
                          margin: EdgeInsets.only(left: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.yellow[700],
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            progressOrder[index1]['status'],
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        FlatButton.icon(
                            onPressed: null,
                            icon: Icon(Icons.timer),
                            label: FittedBox(
                                child: Text(
                              (progressOrder[index1]['time']),
                              style: TextStyle(fontSize: 15),
                            )))
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          padding: EdgeInsets.all(3),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Transaction ID'),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                progressOrder[index1]['transId'],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(3),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Delivered to'),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                progressOrder[index1]['deliveredTo'],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(3),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Total Payment'),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                progressOrder[index1]['total'],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                return OrderStatusScreen(
                  id: progressOrder[index1]['transId'],
                  cost: progressOrder[index1]['total'],
                );
              }));
            },
          )
      ],
    );
  }
}
