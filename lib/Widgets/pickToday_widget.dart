import 'package:flutter/material.dart';
import 'package:grofar/Screens/pickToday_screen.dart';

class PickToday extends StatelessWidget {
  List tiles = [
    {'image': 'assets/img/cart/g1.png', 'item': 'Friuts'},
    {'image': 'assets/img/cart/g2.png', 'item': 'Chili'},
    {'image': 'assets/img/cart/g3.png', 'item': 'Onion'},
    {'image': 'assets/img/cart/g1.png', 'item': 'Cabbage'},
    {'image': 'assets/img/cart/g2.png', 'item': 'Friuts'},
    {'image': 'assets/img/cart/g3.png', 'item': 'Jamun'},
    {'image': 'assets/img/cart/g1.png', 'item': 'Friuts'},
    {'image': 'assets/img/cart/g2.png', 'item': 'Friuts'},
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      //height: deviceSize.height / 2 * .7,
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  'Pick today',
                  style: TextStyle(
                      color: Color(0xFF212529),
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      letterSpacing: .8),
                ),
              ),
              GestureDetector(
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Text(
                    'See More',
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                        letterSpacing: .8),
                  ),
                ),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                    return PickTodayScreen();
                  }));
                },
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Column(children: [
            GridView(
              scrollDirection: Axis.vertical,
              physics: BouncingScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 8.0,
                mainAxisSpacing: 5.0,
                crossAxisCount: 2,
                childAspectRatio: 4 / 4.5,
              ),
              shrinkWrap: true,
              children: <Widget>[
                for (int index1 = 0; index1 < tiles.length; index1++)
                  Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Container(
                        padding: EdgeInsets.all(5),
                        child: Column(children: [
                          Container(
                            width: deviceSize.width / 2 * .6,
                            height: deviceSize.height / 4 * .5,
                            child: Stack(
                              children: [
                                Positioned(
                                    top: 10,
                                    left: 5,
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.orangeAccent[100],
                                      ),
                                      child: Text(
                                        "25%",
                                        style: TextStyle(
                                            color: Colors.orange[700]),
                                      ),
                                    )),
                              ],
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage(tiles[index1]['image']),
                                    fit: BoxFit.fill)),
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(5),
                                  margin: EdgeInsets.only(left: 10, top: 10),
                                  child: Text(
                                    tiles[index1]['item'],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xFF212529),
                                        fontSize: 16),
                                  ),
                                ),
                              ]),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  '\$ 1/kg',
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                              GestureDetector(
                                child: Container(
                                  margin: EdgeInsets.only(right: 10),
                                  width: 40,
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFF0F2F5),
                                    shape: BoxShape.circle,
                                  ),
                                  child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.add,
                                      color: Colors.orange,
                                    ),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )
                        ])),
                  )
              ],
            )
          ])
        ],
      ),
    );
  }
}
