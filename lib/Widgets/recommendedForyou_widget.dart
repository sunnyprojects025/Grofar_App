import 'package:flutter/material.dart';
import 'package:grofar/Screens/recommend_foryou_screen.dart';

class RecommendedForYou extends StatefulWidget {
  @override
  _RecommendedForYouState createState() => _RecommendedForYouState();
}

class _RecommendedForYouState extends State<RecommendedForYou> {
  int counter = 1;
  final List _recommended = [
    {
      'fruit': 'Orange',
      'images': [
        'assets/img/recommend/r1.jpg',
        'assets/img/recommend/r2.jpg',
        'assets/img/recommend/r3.jpg',
      ],
      'details': 'Orange great Quality item from Jamaica'
    },
    {
      'fruit': 'Apple',
      'images': [
        'assets/img/recommend/r4.jpg',
        'assets/img/recommend/r5.jpg',
        'assets/img/recommend/r6.jpg',
        'assets/img/recommend/r7.jpg',
        'assets/img/recommend/r8.jpg',
        'assets/img/recommend/r9.jpg',
      ],
      'details': 'Apple great Quality item from Jamaica'
    },
  ];

  var index1;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
              child: Text(
                'Recommended for You',
                style: TextStyle(
                    color: Color(0xFF212529),
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    letterSpacing: .8),
              ),
            ),
            GestureDetector(
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Text(
                  'See More',
                  style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      letterSpacing: .8),
                ),
              ),
            )
          ],
        ),
        for (int index1 = 0; index1 < _recommended.length; index1++)
          Card(
            margin: EdgeInsets.only(top: 10, bottom: 10),
            child: Container(
              width: double.infinity,
              height: deviceSize.height * .6 * .9,
              padding: EdgeInsets.all(5),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: deviceSize.height / 3 * .9,
                    margin: EdgeInsets.all(8),
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        ..._recommended[index1]['images']
                            .map((item) => GestureDetector(
                                  child: Container(
                                    width: deviceSize.width * .7,
                                    height: deviceSize.height / 3,
                                    margin: EdgeInsets.all(5),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(20),
                                        child: Image.asset(
                                          item,
                                          fit: BoxFit.fill,
                                        )),
                                  ),
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (ctx) {
                                      return RecommendForYouScreen();
                                    }));
                                  },
                                ))
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 10, top: 10),
                    child: Text(
                      _recommended[index1]['fruit'],
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          letterSpacing: .8),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      _recommended[index1]['details'],
                      style: TextStyle(fontSize: 16, letterSpacing: .8),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          '\$ 8/kg',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18),
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(right: deviceSize.width / 4 / 3),
                        padding: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.grey.withOpacity(.2)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              child: Container(
                                width: 30,
                                height: 30,
                                margin: EdgeInsets.only(right: 10),
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.remove,
                                  color: Colors.orange,
                                ),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white),
                              ),
                              onTap: () {
                                setState(() {
                                  counter == 0 ? null : counter--;
                                });
                              },
                            ),
                            Container(
                              child: Text(counter.toString()),
                            ),
                            GestureDetector(
                              child: Container(
                                margin: EdgeInsets.only(left: 10),
                                width: 30,
                                height: 30,
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.add,
                                  color: Colors.orange,
                                ),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white),
                              ),
                              onTap: () {
                                setState(() {
                                  counter++;
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}
