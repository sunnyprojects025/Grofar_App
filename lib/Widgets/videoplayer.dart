import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class BackgroundVideoplayer extends StatefulWidget {
  @override
  _BackgroundVideoplayerState createState() => _BackgroundVideoplayerState();
}

class _BackgroundVideoplayerState extends State<BackgroundVideoplayer> {
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    videoPlayerController = VideoPlayerController.asset(
      "assets/video/backgroundvideo.mp4",
    );
    chewieController = ChewieController(
      showControls: false,
      showControlsOnInitialize: false,
      videoPlayerController: videoPlayerController,
      aspectRatio: MediaQuery.of(context).size.width /
          MediaQuery.of(context).size.height,
      autoPlay: true,
      looping: true,
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    //_chewieController.dispose();
    videoPlayerController.dispose();
    chewieController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Chewie(controller: chewieController);
  }
}
