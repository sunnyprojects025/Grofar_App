import 'package:flutter/material.dart';
import 'package:grofar/Screens/lookingForItem_screen.dart';

class LookingForCard extends StatelessWidget {
  List tiles = [
    {'image': 'assets/img/categorie/1.png', 'footer': 'Friuts'},
    {'image': 'assets/img/categorie/2.png', 'footer': 'vegetable'},
    {'image': 'assets/img/categorie/3.png', 'footer': 'Friuts'},
    {'image': 'assets/img/categorie/4.png', 'footer': 'vegetable'},
    {'image': 'assets/img/categorie/5.png', 'footer': 'Friuts'},
    {'image': 'assets/img/categorie/6.png', 'footer': 'vegetable'},
    {'image': 'assets/img/categorie/7.png', 'footer': 'Friuts'},
    {'image': 'assets/img/categorie/8.png', 'footer': 'vegetable'},
  ];

  var index1;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Column(children: [
      GridView(
        scrollDirection: Axis.vertical,
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.only(left: 10, right: 10),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 6 / 8,
          crossAxisCount: 4,
        ),
        shrinkWrap: true,
        children: <Widget>[
          for (int index1 = 0; index1 < tiles.length; index1++)
            GestureDetector(
              child: Card(
                margin: EdgeInsets.all(5),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: [
                        Container(
                          width: deviceSize.width / 4 / 2,
                          height: deviceSize.height / 4 / 3,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage(tiles[index1]['image']),
                                  fit: BoxFit.fill)),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        FittedBox(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [Text(tiles[index1]['footer'])],
                          ),
                        )
                      ],
                    )),
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                  return LookingForItemScreen(
                    title: tiles[index1]['footer'],
                  );
                }));
              },
            )
        ],
      )
    ]);
  }
}
