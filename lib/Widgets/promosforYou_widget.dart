import 'package:flutter/material.dart';
import 'package:grofar/Screens/promos_page_screen.dart';
import 'package:grofar/Screens/promos_screen.dart';

class PromosForYou extends StatelessWidget {
  final List _pickoffers = [
    'assets/img/promo1.jpg',
    'assets/img/promo2.jpg',
    'assets/img/promo3.jpg',
  ];
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Card(
      child: Container(
        width: double.infinity,
        height: deviceSize.height / 2 * .7,
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Promos for you',
                    style: TextStyle(
                        color: Color(0xFF212529),
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                        letterSpacing: .8),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    margin: EdgeInsets.only(right: 10),
                    child: Text(
                      'See More',
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          letterSpacing: .8),
                    ),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (ctx) {
                      return PromosScreen();
                    }));
                  },
                )
              ],
            ),
            Container(
                width: deviceSize.width,
                height: deviceSize.height / 4,
                margin: EdgeInsets.all(10),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: _pickoffers.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      child: Container(
                        width: deviceSize.width * .7,
                        height: deviceSize.height / 3,
                        margin: EdgeInsets.all(5),
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.asset(
                              _pickoffers[index],
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (ctx) {
                          return PromosPageScreen(
                            image: _pickoffers[index],
                          );
                        }));
                      },
                    );
                  },
                )),
          ],
        ),
      ),
    );
  }
}
